package com.muhammadfadhlidzilikram.apps_mepel_android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.muhammadfadhlidzilikram.apps_mepel_android.R;

public class cara_pemakaian extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cara_pemakaian);
    }

    public void login(View view) {
        Intent login = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.login.DashboardActivity.class);
        startActivity(login);
    }

    public void menu_utama(View view) {
        Intent utama = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.menu_utama.class);
        startActivity(utama);
    }

    public void materi(View view) {
        Intent materi = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.MainActivity.class);
        startActivity(materi);
    }

    public void cara_pakai(View view) {
        Intent cara = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.activities.cara_pemakaian.class);
        startActivity(cara);
    }

    public void info(View view) {
        Intent inf = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.activities.info_apps.class);
        startActivity(inf);
    }

    public void quiz(View view) {
        Intent kuis = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.quiz.HomeActivity.class);
        startActivity(kuis);
    }

    public void tutorial(View view) {
        Intent tutor = new Intent(this, com.muhammadfadhlidzilikram.apps_mepel_android.menu_tutorial.class);
        startActivity(tutor);
    }
}