package com.muhammadfadhlidzilikram.apps_mepel_android.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.muhammadfadhlidzilikram.apps_mepel_android.R;

public class info_apps extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_apps);

        if (savedInstanceState == null) {
            main_me me = new main_me();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nav_host_fragment_main, me)
                    .commit();
        }

        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view_mainn);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigasi_me:
                main_me me = new main_me();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.nav_host_fragment_main, me)
                        .commit();

                return true;

            case R.id.navigasi_apps:
                main_apps apps = new main_apps();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.nav_host_fragment_main, apps)
                        .commit();
                return true;


        }
        return false;
    }


    public void tlp(View view) {
        Intent i = new
                Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://wa.me/6282165368470"));
        startActivity(i);
    }

    public void mail(View view) {
        Intent m = new
                Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("mailto:muhammadfadly.mfd@gmail.com"));
        startActivity(m);
    }

    public void lokasi(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/maps/MvuXtTX86x5wmQ7HA"));
        startActivity(intent);
    }
}