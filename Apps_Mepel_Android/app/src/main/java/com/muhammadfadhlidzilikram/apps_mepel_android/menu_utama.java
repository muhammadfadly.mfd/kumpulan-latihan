package com.muhammadfadhlidzilikram.apps_mepel_android;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.card.MaterialCardView;
import com.muhammadfadhlidzilikram.apps_mepel_android.login.SessionHandler;
import com.muhammadfadhlidzilikram.apps_mepel_android.login.User;

import java.util.Calendar;
import java.util.Date;

public class menu_utama extends Activity implements View.OnClickListener {
    private SessionHandler session;
    MaterialCardView cvmateri, cvTutorial, cvQuiz, cvInfo, cvPemakaian, cvLogut;
    TextView tvToday;

    String hariIni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        session = new SessionHandler(getApplicationContext());
        User user = session.getUserDetails();

        TextView welcomeTex = findViewById(R.id.welcomeText);
        welcomeTex.setText("Selamat Datang " + user.getFullName());


        cvmateri = findViewById(R.id.materi);
        cvTutorial = findViewById(R.id.tutorial);
        cvQuiz = findViewById(R.id.quiz);
        cvInfo = findViewById(R.id.info);
        cvPemakaian = findViewById(R.id.pemakaian);
        cvLogut = findViewById(R.id.logut);

        cvmateri.setOnClickListener(this);
        cvTutorial.setOnClickListener(this);
        cvQuiz.setOnClickListener(this);
        cvInfo.setOnClickListener(this);
        cvPemakaian.setOnClickListener(this);
        cvLogut.setOnClickListener(this);

        tvToday = findViewById(R.id.tvDate);
        Date dateNow = Calendar.getInstance().getTime();
        hariIni = (String) DateFormat.format("EEEE", dateNow);
        if (hariIni.equalsIgnoreCase("sunday")) {
            hariIni = "Minggu";
        } else if (hariIni.equalsIgnoreCase("monday")) {
            hariIni = "Senin";
        } else if (hariIni.equalsIgnoreCase("tuesday")) {
            hariIni = "Selasa";
        } else if (hariIni.equalsIgnoreCase("wednesday")) {
            hariIni = "Rabu";
        } else if (hariIni.equalsIgnoreCase("thursday")) {
            hariIni = "Kamis";
        } else if (hariIni.equalsIgnoreCase("friday")) {
            hariIni = "Jumat";
        } else if (hariIni.equalsIgnoreCase("saturday")) {
            hariIni = "Sabtu";
        }

        getToday();

    }

    public void onBackPressed() {
        CFAlertDialog.Builder alertDialogBuilder = new CFAlertDialog.Builder(this);
        alertDialogBuilder.setIcon(R.drawable.unimal);
        alertDialogBuilder.setTitle("Belajar Untuk Hebat");
        alertDialogBuilder.setMessage("Kamu Yakin Ngak Mau Belajar?")
                .addButton("IYA DONKS!!", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                    dialog.dismiss();
                    System.exit(0);
                });

        alertDialogBuilder.create().show();
    }

    private void getToday() {
        Date date = Calendar.getInstance().getTime();
        String tanggal = (String) DateFormat.format("d", date);
        String monthNumber = (String) DateFormat.format("M", date);
        String year = (String) DateFormat.format("yyyy", date);

        int month = Integer.parseInt(monthNumber);
        String bulan = null;
        if (month == 1) {
            bulan = "Januari";
        } else if (month == 2) {
            bulan = "Februari";
        } else if (month == 3) {
            bulan = "Maret";
        } else if (month == 4) {
            bulan = "April";
        } else if (month == 5) {
            bulan = "Mei";
        } else if (month == 6) {
            bulan = "Juni";
        } else if (month == 7) {
            bulan = "Juli";
        } else if (month == 8) {
            bulan = "Agustus";
        } else if (month == 9) {
            bulan = "September";
        } else if (month == 10) {
            bulan = "Oktober";
        } else if (month == 11) {
            bulan = "November";
        } else if (month == 12) {
            bulan = "Desember";
        }
        String formatFix = hariIni + ", " + tanggal + " " + bulan + " " + year;
        tvToday.setText(formatFix);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.materi) {
            Intent intent = new Intent(menu_utama.this, MainActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.tutorial) {
            Intent intent = new Intent(menu_utama.this, menu_tutorial.class);
            startActivity(intent);
        } else if (v.getId() == R.id.quiz) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.apps_mepel_android.quiz.HomeActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.info) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.apps_mepel_android.activities.info_apps.class);
            startActivity(intent);
        } else if (v.getId() == R.id.pemakaian) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.apps_mepel_android.activities.cara_pemakaian.class);
            startActivity(intent);
        } else if (v.getId() == R.id.logut) {

            CFAlertDialog.Builder alertDialogBuilder = new CFAlertDialog.Builder(this);
            alertDialogBuilder.setIcon(R.drawable.unimal);
            alertDialogBuilder.setTitle("Belajar Untuk Hebat");
            alertDialogBuilder.setMessage("Kamu Yakin Keluar Dari Aplikasi")
                    .addButton("IYA !!!", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                        dialog.dismiss();
                        session.logoutUser();
                        Intent intent = new Intent(
                        menu_utama.this, com.muhammadfadhlidzilikram.apps_mepel_android.login.LoginActivity.class);
                        startActivity(intent);
                    });

            alertDialogBuilder.create().show();

        }
    }
}