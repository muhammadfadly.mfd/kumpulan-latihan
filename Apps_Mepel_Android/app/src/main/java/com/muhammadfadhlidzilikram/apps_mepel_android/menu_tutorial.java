package com.muhammadfadhlidzilikram.apps_mepel_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.muhammadfadhlidzilikram.apps_mepel_android.login.SessionHandler;
import com.muhammadfadhlidzilikram.apps_mepel_android.login.User;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.activityActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.androidActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.deploymentActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.exdatabaseActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.firebaseActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.fragmentActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.kontenActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.multimediaActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.networkActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.persistanceActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.sensorActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.serviceActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.sqlActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.userActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.tutorial.pengenalanActivity;

import java.util.Calendar;
import java.util.Date;

public class menu_tutorial extends Activity implements View.OnClickListener {
    private SessionHandler session;
    MaterialCardView cvPengenalan, cvAndroid, cvactivity, cvFragment, cvUser, cvContent, cvSql, cvPersistance, cvNetwork, cvMultimedia, cvExdatabase, cvService, cvFirebase, cvDeployment, cvSensor;
    TextView tvToday;

    String hariIni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_tutorial);

        session = new SessionHandler(getApplicationContext());
        User user = session.getUserDetails();

        TextView welcomeTex = findViewById(R.id.welcomeText);
        welcomeTex.setText("Selamat Datang "+user.getFullName());


        cvPengenalan = findViewById(R.id.pengenalan);
        cvAndroid = findViewById(R.id.android);
        cvactivity = findViewById(R.id.activity);
        cvFragment = findViewById(R.id.fragment);
        cvUser = findViewById(R.id.user);
        cvContent = findViewById(R.id.konten);
        cvSql = findViewById(R.id.sql);
        cvPersistance = findViewById(R.id.persistance);
        cvNetwork = findViewById(R.id.network);
        cvMultimedia = findViewById(R.id.multimedia);
        cvExdatabase = findViewById(R.id.exdatabase);
        cvService = findViewById(R.id.service);
        cvFirebase = findViewById(R.id.firebase);
        cvDeployment = findViewById(R.id.deployment);
        cvSensor = findViewById(R.id.sensor);


        cvPengenalan.setOnClickListener(this);
        cvAndroid.setOnClickListener(this);
        cvactivity.setOnClickListener(this);
        cvFragment.setOnClickListener(this);
        cvUser.setOnClickListener(this);
        cvContent.setOnClickListener(this);
        cvSql.setOnClickListener(this);
        cvPersistance.setOnClickListener(this);
        cvNetwork.setOnClickListener(this);
        cvMultimedia.setOnClickListener(this);
        cvExdatabase.setOnClickListener(this);
        cvService.setOnClickListener(this);
        cvFirebase.setOnClickListener(this);
        cvDeployment.setOnClickListener(this);
        cvSensor.setOnClickListener(this);

        tvToday = findViewById(R.id.tvDate);
        Date dateNow = Calendar.getInstance().getTime();
        hariIni = (String) DateFormat.format("EEEE", dateNow);
        if (hariIni.equalsIgnoreCase("sunday")) {
            hariIni = "Minggu";
        } else if (hariIni.equalsIgnoreCase("monday")) {
            hariIni = "Senin";
        } else if (hariIni.equalsIgnoreCase("tuesday")) {
            hariIni = "Selasa";
        } else if (hariIni.equalsIgnoreCase("wednesday")) {
            hariIni = "Rabu";
        } else if (hariIni.equalsIgnoreCase("thursday")) {
            hariIni = "Kamis";
        } else if (hariIni.equalsIgnoreCase("friday")) {
            hariIni = "Jumat";
        } else if (hariIni.equalsIgnoreCase("saturday")) {
            hariIni = "Sabtu";
        }

        getToday();

    }

    private void getToday() {
        Date date = Calendar.getInstance().getTime();
        String tanggal = (String) DateFormat.format("d", date);
        String monthNumber = (String) DateFormat.format("M", date);
        String year = (String) DateFormat.format("yyyy", date);

        int month = Integer.parseInt(monthNumber);
        String bulan = null;
        if (month == 1) {
            bulan = "Januari";
        } else if (month == 2) {
            bulan = "Februari";
        } else if (month == 3) {
            bulan = "Maret";
        } else if (month == 4) {
            bulan = "April";
        } else if (month == 5) {
            bulan = "Mei";
        } else if (month == 6) {
            bulan = "Juni";
        } else if (month == 7) {
            bulan = "Juli";
        } else if (month == 8) {
            bulan = "Agustus";
        } else if (month == 9) {
            bulan = "September";
        } else if (month == 10) {
            bulan = "Oktober";
        } else if (month == 11) {
            bulan = "November";
        } else if (month == 12) {
            bulan = "Desember";
        }
        String formatFix = hariIni + ", " + tanggal + " " + bulan + " " + year;
        tvToday.setText(formatFix);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.pengenalan) {
            Intent intent = new Intent(menu_tutorial.this, pengenalanActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.android) {
            Intent intent = new Intent(menu_tutorial.this, androidActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.activity) {
            Intent intent = new Intent(menu_tutorial.this, activityActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.fragment) {
            Intent intent = new Intent(menu_tutorial.this, fragmentActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.user) {
            Intent intent = new Intent(menu_tutorial.this, userActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.konten) {
            Intent intent = new Intent(menu_tutorial.this, kontenActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.sql) {
            Intent intent = new Intent(menu_tutorial.this, sqlActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.persistance) {
            Intent intent = new Intent(menu_tutorial.this, persistanceActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.network) {
            Intent intent = new Intent(menu_tutorial.this, networkActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.multimedia) {
            Intent intent = new Intent(menu_tutorial.this, multimediaActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.exdatabase) {
            Intent intent = new Intent(menu_tutorial.this, exdatabaseActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.service) {
            Intent intent = new Intent(menu_tutorial.this, serviceActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.firebase) {
            Intent intent = new Intent(menu_tutorial.this, firebaseActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.deployment) {
            Intent intent = new Intent(menu_tutorial.this, deploymentActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.sensor) {
            Intent intent = new Intent(menu_tutorial.this, sensorActivity.class);
            startActivity(intent);
        }
    }

}