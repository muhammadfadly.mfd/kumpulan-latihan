package com.muhammadfadhlidzilikram.apps_mepel_android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.muhammadfadhlidzilikram.apps_mepel_android.R;
import com.muhammadfadhlidzilikram.apps_mepel_android.quiz.admin.MainActivity;

public class HomeActivity extends AppCompatActivity {

    Button start,admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        start = findViewById(R.id.start);


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),QuizActivity.class));
            }
        });


    }
}
