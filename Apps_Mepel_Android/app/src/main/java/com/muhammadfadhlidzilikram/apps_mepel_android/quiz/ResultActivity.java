package com.muhammadfadhlidzilikram.apps_mepel_android.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.muhammadfadhlidzilikram.apps_mepel_android.R;

import static android.graphics.Color.BLUE;
import static android.graphics.Color.RED;
import static android.graphics.Color.YELLOW;
import static android.graphics.Color.blue;
import static android.graphics.Color.green;

public class ResultActivity extends AppCompatActivity {


    TextView tq,ca,wa,hu,motivasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);



        motivasi = findViewById(R.id.motiv);
        tq = findViewById(R.id.tq);
        ca=findViewById(R.id.ca);
        wa=findViewById(R.id.wa);
        hu=findViewById(R.id.hu);
        Intent i=getIntent();
        String questions=i.getStringExtra("total");
        String correct=i.getStringExtra("correct");
        String hint=i.getStringExtra("hint");
        String wrong=i.getStringExtra("wrong");
        tq.setText(questions);
        ca.setText(correct);
        hu.setText(hint);
        wa.setText(wrong);

        double motiv = Double.parseDouble(ca.getText().toString());
        if (motiv >= 17) {
            motivasi.setText("Indonesia Butuh Orang Seperti Kamu, Lanjutkan Kawan, Tetap Giat Belajar Ya!");
            motivasi.setTextColor(BLUE);

        } else if (motiv >= 10) {
            motivasi.setText("Nilai Kamu Standart Aja Makanya Belajar Yang Rajin Lagi");

        } else {
            motivasi.setText("Nilai Kamu Benar-Benar jelek ya kawan, Gih BELAJAR SANA!!!");
            motivasi.setTextColor(RED);

        }
    }
}
