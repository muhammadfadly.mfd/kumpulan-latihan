package com.muhammadfadhlidzilikram.apps_mepel_android.api;

public class NewsApi {
    public static String GET_TOP_HEADLINES = "https://androidstack.000webhostapp.com/materi/retrieve_androidstack.php";
    public static String GET_CATEGORY_SPORTS = "https://androidstack.000webhostapp.com/materi/retrieve_androidstack.php";
    public static String GET_CATEGORY_TECHNOLOGY = "https://androidstack.000webhostapp.com/materi/retrieve_androidstack.php";
    public static String GET_CATEGORY_BUSINESS = "https://newsapi.org/v2/top-headlines?country=id&category=business&apiKey=f3aa09e2f1d7494e9fded68f30d4e4c1";
    public static String GET_CATEGORY_HEALTH = "https://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=f3aa09e2f1d7494e9fded68f30d4e4c1";
    public static String GET_CATEGORY_ENTERTAINMENT = "https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=f3aa09e2f1d7494e9fded68f30d4e4c1";

    //    MATERI
    public static String GET_MATERI_PENGENALAN = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_pengenalan.php";
    public static String GET_MATERI_ANDROID = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_android_studio.php";
    public static String GET_MATERI_ACTIVITY = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_activity.php";
    public static String GET_MATERI_FRAGMENT = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_fragment.php";
    public static String GET_MATERI_USER = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_user_interface.php";
    public static String GET_MATERI_KONTENT = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_content_provider.php";
    public static String GET_MATERI_SQL = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_sqlite_database.php";
    public static String GET_MATERI_PERSISTANCE = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_data_persistance.php";
    public static String GET_MATERI_NETWORK = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_komunikasi.php";
    public static String GET_MATERI_MULTIMEDIA = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_android_multimedia.php";
    public static String GET_MATERI_EXDATABASE = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_external_database.php";
    public static String GET_MATERI_SERVICE = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_service.php";
    public static String GET_MATERI_FIREBASE = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_firebase.php";
    public static String GET_MATERI_DEPLOYMENT = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_deployment.php";
    public static String GET_MATERI_SENSOR = "https://androidstack.000webhostapp.com/Androidstack/api/materi/retrieve_android_sensor.php";


    //    TUTORIAL
    public static String GET_TUTORIAL_PENGENALAN = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_pengenalan.php";
    public static String GET_TUTORIAL_ANDROID = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_android_studio.php";
    public static String GET_TUTORIAL_ACTIVITY = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_activity.php";
    public static String GET_TUTORIAL_FRAGMENT = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_fragment.php";
    public static String GET_TUTORIAL_USER = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_user_interface.php";
    public static String GET_TUTORIAL_KONTENT = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_content_provider.php";
    public static String GET_TUTORIAL_SQL = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_sqlite_database.php";
    public static String GET_TUTORIAL_PERSISTANCE = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_data_persistance.php";
    public static String GET_TUTORIAL_NETWORK = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_komunikasi.php";
    public static String GET_TUTORIAL_MULTIMEDIA = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_android_multimedia.php";
    public static String GET_TUTORIAL_EXDATABASE = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_external_database.php";
    public static String GET_TUTORIAL_SERVICE = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_service.php";
    public static String GET_TUTORIAL_FIREBASE = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_firebase.php";
    public static String GET_TUTORIAL_DEPLOYMENT = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_deployment.php";
    public static String GET_TUTORIAL_SENSOR = "https://androidstack.000webhostapp.com/Androidstack/api/tutorial/retrieve_android_sensor.php";
}