package com.muhammadfadhlidzilikram.apps_mepel_android.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.muhammadfadhlidzilikram.apps_mepel_android.MainActivity;
import com.muhammadfadhlidzilikram.apps_mepel_android.R;
import com.muhammadfadhlidzilikram.apps_mepel_android.awal.PrefManager;
import com.muhammadfadhlidzilikram.apps_mepel_android.awal.splasscren;
import com.muhammadfadhlidzilikram.apps_mepel_android.menu_utama;

public class DashboardActivity extends Activity {
    private SessionHandler session;
    private PrefManager prefManager;
    private int waktu_loading = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        session = new SessionHandler(getApplicationContext());
        User user = session.getUserDetails();
        TextView welcomeText = findViewById(R.id.welcomeText);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //setelah loading maka akan langsung berpindah ke home activity
                Intent home = new Intent(DashboardActivity.this,
                        menu_utama.class);
                startActivity(home);
                finish();
            }
        }, waktu_loading);

//        prefManager = new PrefManager(this);
//        if (!prefManager.isFirstTimeLaunch()) {
//            launchHomeScreen();
//            finish();
//        }

        welcomeText.setText("Selamat Datang "+user.getFullName()+", Kamu akan keluar dari aplikasi pada "+user.getSessionExpiryDate()+" secara otomatis akan dialihkan ke menu login");

        Button logoutBtn = findViewById(R.id.btnLogout);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logoutUser();
                Intent i = new Intent(com.muhammadfadhlidzilikram.apps_mepel_android.login.DashboardActivity.this, com.muhammadfadhlidzilikram.apps_mepel_android.login.LoginActivity.class);
                startActivity(i);
                finish();

            }
        });

        Button masuk = findViewById(R.id.btnmasuk);

        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.isLoggedIn();
                Intent i = new Intent(com.muhammadfadhlidzilikram.apps_mepel_android.login.DashboardActivity.this, com.muhammadfadhlidzilikram.apps_mepel_android.awal.splasscren.class);
                startActivity(i);
                finish();

            }
        });
    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(DashboardActivity.this,
                LoginActivity.class));
        finish();
    }
}
