package com.muhammadfadhlidzilikram.apps_mepel_android.quiz.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.muhammadfadhlidzilikram.apps_mepel_android.R;

import java.util.ArrayList;


public class AdapterAbsenRecyclerView extends RecyclerView.Adapter<ItemAbsenViewHolder>
{
	private Context context;
	private ArrayList<Quiz> daftarQuiz;
	private FirebaseDataListener listener;
	
	public AdapterAbsenRecyclerView(Context context, ArrayList<Quiz> daftarQuiz){
		this.context = context;
		this.daftarQuiz = daftarQuiz;
		this.listener = (FirebaseDataListener)context;
	}

	@Override
	public ItemAbsenViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		// TODO: Implement this method
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quiz, parent, false);
		ItemAbsenViewHolder holder = new ItemAbsenViewHolder(view);
		return holder;
	}

	@Override
	public void onBindViewHolder(ItemAbsenViewHolder holder, final int position)
	{
		// TODO: Implement this method
		holder.soalQuiz.setText("Soal Quiz\t\t\t\t\t:\t "+ daftarQuiz.get(position).getQuestion());
		holder.jawabanQuiz.setText("Jawaban Quiz\t:\t "+ daftarQuiz.get(position).getAnswer());

		
		holder.view.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				listener.onDataClick(daftarQuiz.get(position), position);
			}
		});
	}

	@Override
	public int getItemCount()
	{
		// TODO: Implement this method
		return daftarQuiz.size();
	}
	
	
	//interface data listener
	public interface FirebaseDataListener {
		void onDataClick(Quiz quiz, int position);
	}
}
