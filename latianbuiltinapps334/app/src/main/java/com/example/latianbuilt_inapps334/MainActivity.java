package com.example.latianbuilt_inapps334;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void map(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/maps/aXXsePvgYVdXUfmK8"));
        startActivity(i);
    }

    public void web(View view) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/fadly_dzil_ikrom/"));
            startActivity(i);
    }

    public void telpo(View view) {
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+6282165368470"));
        startActivity(i);
    }
}
