package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v3.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v3.Fragment.chart_kelembapan;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v3.Fragment.kelembapan;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v3.R;


public class f_kelembapan extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_kelembapan, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            kelembapan ff_kelembapan = new kelembapan();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_kelembapan, ff_kelembapan)
                    .commit();
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.nav_view_kelembapan);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data1:
                kelembapan ff_kelembapan = new kelembapan();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_kelembapan, ff_kelembapan)
                        .commit();
                return true;

            case R.id.navigation_chart1:
                chart_kelembapan chart_kelembapan = new chart_kelembapan();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_kelembapan, chart_kelembapan)
                        .commit();
                return true;

        }
        return false;
    }

}
