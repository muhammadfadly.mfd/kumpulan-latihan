package com.fadlidzilikram.laundry.API;

import com.fadlidzilikram.laundry.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIrequestData {
    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();
}
