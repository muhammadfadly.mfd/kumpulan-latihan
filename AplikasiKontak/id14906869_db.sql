-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 24, 2020 at 01:21 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id14906869_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_aduan`
--

CREATE TABLE `tb_aduan` (
  `notiket` int(11) NOT NULL,
  `tipe` text COLLATE utf8_unicode_ci NOT NULL,
  `unitkerja` text COLLATE utf8_unicode_ci NOT NULL,
  `penghubung` text COLLATE utf8_unicode_ci NOT NULL,
  `notelp` text COLLATE utf8_unicode_ci NOT NULL,
  `emailp` text COLLATE utf8_unicode_ci NOT NULL,
  `emaila` text COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `aksi` text COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `posisi` varchar(100) NOT NULL,
  `gajih` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id`, `nama`, `posisi`, `gajih`) VALUES
(1, 'Febri', 'Manager', '7000000'),
(2, 'Edi', 'PM', '450000'),
(3, 'fadli', 'super manager', '6373891'),
(5, 'rinal', 'super manager', '6373891'),
(6, 'ajib', 'dekan', '27388484'),
(7, 'ggg', 'ccc', '4667'),
(8, 'manuak', 'orang biasa', '1'),
(10, 'fred', 'fffh tf', 'fffdd'),
(12, 'tia', '1', '200'),
(13, '', '', ''),
(14, 'maulana', 'CEO', '$100000'),
(15, '', '', ''),
(16, 'wahyu', 'manager', '100jt'),
(17, '', '', ''),
(18, '', '', ''),
(19, '', '', ''),
(20, 'mama', 'mamam', '1223'),
(21, 'mama', 'mamam', '1223');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_aduan`
--
ALTER TABLE `tb_aduan`
  ADD PRIMARY KEY (`notiket`);

--
-- Indexes for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_aduan`
--
ALTER TABLE `tb_aduan`
  MODIFY `notiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
