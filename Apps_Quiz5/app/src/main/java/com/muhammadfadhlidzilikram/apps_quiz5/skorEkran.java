package com.muhammadfadhlidzilikram.apps_quiz5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class skorEkran extends AppCompatActivity {
    public TextView skor;
    public TextView yanlisSayi;
    public int yanlisCek;
    Context context = this;
    public Button yeniden, cikis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skor_ekran);

        skor = (TextView) findViewById(R.id.tvSkor);
        yanlisSayi = (TextView) findViewById(R.id.tvYanlis);

        Intent soruEkrani = getIntent();
        String skorCek=soruEkrani.getStringExtra("skor");
        skor.setText(skorCek);

        yanlisCek = (10 - (Integer.parseInt(skor.getText().toString())));

        yanlisSayi.setText(yanlisCek+"");

        yeniden = (Button) findViewById(R.id.btnYeniden);
        cikis = (Button) findViewById(R.id.btnYeniden2);

        yeniden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent anaEkranDonus = new Intent(skorEkran.this,MainActivity.class);
                startActivity(anaEkranDonus);
            }
        });

        cikis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // MESAJ
                AlertDialog.Builder cikisMesaj = new AlertDialog.Builder(context);
                cikisMesaj.setTitle("Uygulamadan çıkış yapmak istediğinizden emin misiniz ?")
                        .setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                //finish();
                                //evet
                                //finish();
                                finishAffinity();//uygulamayı tamamen kapatma
                                System.exit(0);
                            }
                        }).setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).create().show();






            }
        });



    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder cikisMesaj = new AlertDialog.Builder(context);
            cikisMesaj.setTitle("Ana ekrana dönmek istediğinizden emin misiniz ?")
                    .setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                            Intent skorDonus = new Intent(skorEkran.this,MainActivity.class);
                            startActivity(skorDonus);
                            //evet
                            //finish();
                            //finishAffinity();//uygulamayı tamamen kapatma
                            //System.exit(0);
                        }
                    }).setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            }).create().show();
        }
        return super.onKeyDown(keyCode, event);
    }
}
