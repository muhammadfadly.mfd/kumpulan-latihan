package com.muhammadfadhlidzilikram.apps_quiz5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class SoruEkraniActivity extends AppCompatActivity {
    public Button btn1,btn2,btn3,btn4;
    public TextView soruSayisi,tvSan;
    public TextView SoruAciklama;
    public int skor;
    public   int kalanZaman = 31000;
    public CountDownTimer saniyeSayac = null;
    Context context = this; // ÇIKIŞ KONTROL
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.soru_ekrani);

        btn1 = (Button) findViewById(R.id.button);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        tvSan = (TextView) findViewById(R.id.tvSaniye);
        soruSayisi = (TextView) findViewById(R.id.tvSoruSayi);
        skor = 0;
        SoruAciklama = (TextView) findViewById(R.id.tvSoruAciklama);
            soruCek((soruSayisi.getText().toString()));


          startTimer();










        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn1.getTag() == "d" )
                    skor++;

                soruSayisi.setText((Integer.parseInt(soruSayisi.getText().toString())+1)+"");

                if(Integer.parseInt(soruSayisi.getText().toString())>10)
                {
                    //skor ekranına gidecek
                    Intent skorEkran1 = new Intent(SoruEkraniActivity.this,skorEkran.class ); // main activty yerine skor ekranı yazılacak
                    skorEkran1.putExtra("skor",skor+"");
                    startActivity(skorEkran1);

                }
                else{
                    soruCek((soruSayisi.getText().toString()));
                    soruAl((soruSayisi.getText().toString()));
                    cancelTimer();
                    sureGuncelle();
                    startTimer();


                }


            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn2.getTag() == "d" )
                    skor++;

                soruSayisi.setText((Integer.parseInt(soruSayisi.getText().toString())+1)+"");

                if(Integer.parseInt(soruSayisi.getText().toString())>10)
                {
                    //skor ekranına gidecek
                    Intent skorEkran1 = new Intent(SoruEkraniActivity.this,skorEkran.class ); // main activty yerine skor ekranı yazılacak
                    skorEkran1.putExtra("skor",skor+"");
                    startActivity(skorEkran1);

                }
                else{
                    soruCek((soruSayisi.getText().toString()));
                    soruAl((soruSayisi.getText().toString()));
                    cancelTimer();
                    sureGuncelle();
                    startTimer();


                }

            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(btn3.getTag() == "d" )
                    skor++;

                soruSayisi.setText((Integer.parseInt(soruSayisi.getText().toString())+1)+"");

                if(Integer.parseInt(soruSayisi.getText().toString())>10)
                {
                    //skor ekranına gidecek
                    Intent skorEkran1 = new Intent(SoruEkraniActivity.this,skorEkran.class ); // main activty yerine skor ekranı yazılacak
                    skorEkran1.putExtra("skor",skor+"");
                    startActivity(skorEkran1);

                }
                else{
                    soruCek((soruSayisi.getText().toString()));
                    soruAl((soruSayisi.getText().toString()));
                    cancelTimer();
                    sureGuncelle();
                    startTimer();


                }



            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn4.getTag() == "d" )
                    skor++;

                soruSayisi.setText((Integer.parseInt(soruSayisi.getText().toString())+1)+"");

                if(Integer.parseInt(soruSayisi.getText().toString())>10)
                {
                    //skor ekranına gidecek
                    Intent skorEkran1 = new Intent(SoruEkraniActivity.this,skorEkran.class ); // main activty yerine skor ekranı yazılacak
                    skorEkran1.putExtra("skor",skor+"");

                    startActivity(skorEkran1);

                }
                else{
                    soruCek((soruSayisi.getText().toString()));
                    soruAl((soruSayisi.getText().toString()));
                    cancelTimer();
                    sureGuncelle();
                    startTimer();


                }

            }
        });







    }
    public void soruCek(final String soru2)
    {


        String url = "https://pubghintergrund.xyz/gonder.php";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("hata", "onResponse: "+response);
                        final String cvp1,cvp2,cvp3,cvp4;
                        int rand;
                        Random rnd = new Random();
                        rand = rnd.nextInt(4) +1;


                        cvp1 = response.split("-0%")[0];
                        cvp2 = response.split("-0%")[1];
                        cvp3 = response.split("-0%")[2];
                        cvp4 = response.split("-0%")[3];


                        if(rand == 1)
                        {
                            btn1.setTag("d");
                            btn2.setTag("y");
                            btn3.setTag("y");
                            btn4.setTag("y");

                            btn1.setText(cvp1);
                            btn2.setText(cvp2);
                            btn3.setText(cvp3);
                            btn4.setText(cvp4);
                        }
                        if(rand == 2)
                        {
                            btn1.setTag("y");
                            btn2.setTag("d");
                            btn3.setTag("y");
                            btn4.setTag("y");

                            btn1.setText(cvp2);
                            btn2.setText(cvp1);
                            btn3.setText(cvp3);
                            btn4.setText(cvp4);
                        }
                        if(rand == 3)
                        {
                            btn1.setTag("y");
                            btn2.setTag("y");
                            btn3.setTag("d");
                            btn4.setTag("y");

                            btn1.setText(cvp3);
                            btn2.setText(cvp2);
                            btn3.setText(cvp1);
                            btn4.setText(cvp4);
                        }
                        if(rand == 4)
                        {
                            btn1.setTag("y");
                            btn2.setTag("y");
                            btn3.setTag("y");
                            btn4.setTag("d");

                            btn1.setText(cvp4);
                            btn2.setText(cvp2);
                            btn3.setText(cvp3);
                            btn4.setText(cvp1);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("soru", soru2+"");
                return params;
            }
        };
        queue.add(postRequest);
    }
    public void soruAl(final String soru2)
    {


        String url = "https://pubghintergrund.xyz/sorucek.php";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("hata", "onResponse: "+response);

                        SoruAciklama.setText(response);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("soru", soru2+"");
                return params;
            }
        };
        queue.add(postRequest);
    }
    void startTimer() {
        saniyeSayac = new CountDownTimer(kalanZaman, 1000) {
            public void onTick(long millisUntilFinished) {

                tvSan.setText("" + millisUntilFinished / 1000+" sn");


            }
            public void onFinish() {

                soruSayisi.setText((Integer.parseInt(soruSayisi.getText().toString())+1)+"");
                if(Integer.parseInt(soruSayisi.getText().toString())>10)
                {
                    cancelTimer();
                    Intent skorEkran1 = new Intent(SoruEkraniActivity.this,skorEkran.class );
                    skorEkran1.putExtra("skor",skor+"");
                    startActivity(skorEkran1);

                }
                else{
                    tvSan.setText("0 sn");
                    cancelTimer();
                    startTimer();
                    soruCek((soruSayisi.getText().toString()));
                    soruAl((soruSayisi.getText().toString()));
                    Toast.makeText(getApplicationContext(),"Soruyu cevaplamadığınız için diğer soruya geçildi.",Toast.LENGTH_LONG).show();
                    sureGuncelle();
                }


            }
        };
        saniyeSayac.start();
    }


    //cancel timer
    void cancelTimer() {
        if(saniyeSayac!=null)
            saniyeSayac.cancel();
    }
    void sureGuncelle(){


        kalanZaman = 31000;
        //updateCountDownText();
        //int minutes = (int) (30000 / 1000) / 60;
        //int seconds = (int) (30000 / 1000) % 60;



    }
    private void updateCountDownText() {
        //int minutes = (int) (kalanZaman / 1000) / 60;
        //int seconds = (int) (kalanZaman / 1000) % 60;


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder cikisMesaj = new AlertDialog.Builder(context);
            cikisMesaj.setTitle("Ana ekrana dönmek istediğinizden emin misiniz ?")
                    .setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                            Intent soruEkranDonus = new Intent(SoruEkraniActivity.this,MainActivity.class);
                            startActivity(soruEkranDonus);
                            //evet
                            //finish();
                            //finishAffinity();//uygulamayı tamamen kapatma
                            //System.exit(0);
                        }
                    }).setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            }).create().show();
        }
        return super.onKeyDown(keyCode, event);
    }

}