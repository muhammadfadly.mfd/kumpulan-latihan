package com.muhammadfadhlidzilikram.apps_quiz5;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

public class NasilOynanir extends TutorialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_nasil_oynanir);

        setPrevText("Geri"); // Previous button text
        setNextText("İleri"); // Next button text
        setFinishText("Bitir"); // Finish button text
        setCancelText("Vazgeç"); // Cancel button text

        addFragment(new Step.Builder().setTitle("Nasıl oynanır ?")
                .setContent("Oyuna başlamak için ana ekranda bulunan 'Hemen Oyna' butonuna tıklayın. ")
                .setDrawable(R.drawable.btnhemenoyna)
                .setBackgroundColor(Color.parseColor("#34495e"))
                .setSummary("Sonraki adıma geçebilirsiniz.")
                .build());

        addFragment(new Step.Builder().setTitle("Nasıl oynanır ?")
                .setContent("'Hemen Oyna' butonuna tıkladıktan sonra soru ekranı açılacaktır. Soru ekranında bulunan soruları belirtilen süre içerisinde tamamlamanız istenmektedir." +
                        " Belirtilen süre içerisinde cevaplanmayan soru yanlış sayılır ve bir sonraki soruya geçilir.")
                .setDrawable(R.drawable.kirpsoruekran)

                .setBackgroundColor(Color.parseColor("#34495e"))
                .setSummary("Sonraki adıma geçebilirsiniz.")
                .build());

        addFragment(new Step.Builder().setTitle("Nasıl oynanır ?")
                .setContent("Belirtilen süre içerisinde soruyu cevaplamazsanız, soru yanlış sayılır ve diğer soruya geçilir.")
                .setDrawable(R.drawable.soruekrani3)
                .setBackgroundColor(Color.parseColor("#34495e"))
                .setSummary("Sonraki adıma geçebilirsiniz.")
                .build());


        addFragment(new Step.Builder().setTitle("Nasıl oynanır ?")
                .setContent("Sorular bittikten sonra sonuç ekranından doğru ve yanlış soru sayısını görebilirsiniz.")
                .setBackgroundColor(Color.parseColor("#34495e"))
                .setDrawable(R.drawable.bitisekrani)
                .setSummary("Sonraki adıma geçebilirsiniz.")
                .build());


        addFragment(new Step.Builder().setTitle("Nasıl oynanır ?")
                .setContent("Sonuç ekranından tekrar oyuna başlayabilir veya uygulamadan çıkış yapabilirsiniz.")
                .setBackgroundColor(Color.parseColor("#34495e"))
                .setDrawable(R.drawable.yeniden)
                .build());






    }

    @Override
    public void currentFragmentPosition(int position) {

    }
}
