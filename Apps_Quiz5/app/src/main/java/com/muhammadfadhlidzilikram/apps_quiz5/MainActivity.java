package com.muhammadfadhlidzilikram.apps_quiz5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

/*import android.content.Intent;*/
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Button oyna = (Button) findViewById(R.id.btnHemen);
        final Button nasilOynanir = (Button) findViewById(R.id.btnHemen2);

        oyna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent soruEkran = new Intent(MainActivity.this,SoruEkraniActivity.class);
                if(InternetKontrol()){
                    startActivity(soruEkran);

                }else{
                    Toast.makeText(getApplicationContext(),"İnternet bağlantınız yok.",Toast.LENGTH_LONG).show();
                }


            }
        });
        nasilOynanir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nasilOynanirEkran = new Intent(MainActivity.this,NasilOynanir.class);
                startActivity(nasilOynanirEkran);

            }
        });



    }

    public boolean InternetKontrol() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null
                && manager.getActiveNetworkInfo().isAvailable()
                && manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            AlertDialog.Builder cikisMesaj = new AlertDialog.Builder(context);
            cikisMesaj.setTitle("Çıkmak istediğinizden emin misiniz ?")
                    .setPositiveButton("Evet", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //evet
                            //finish();
                             finishAffinity();//uygulamayı tamamen kapatma
                             System.exit(0);

                        }
                    }).setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            }).create().show();
        }
        return super.onKeyDown(keyCode, event);
    }

}

