package com.muhammadfadhlidzilikram.apps_quiz_firebase.admin;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.muhammadfadhlidzilikram.apps_quiz_firebase.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.AdapterAbsenRecyclerView.FirebaseDataListener
{
	//variabel fields
	private Toolbar mToolbar;
	private FloatingActionButton mFloatingActionButton;
	private EditText mEditSoal;
	private EditText mEditOp1;
	private EditText mEditOp2;
	private EditText mEditOp3;
	private EditText mEditOp4;
	private EditText mEditJawaban;
	private RecyclerView mRecyclerView;
	private com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.AdapterAbsenRecyclerView mAdapter;
	private ArrayList<Quiz> daftarQuiz;

	Calendar myCalendar = Calendar.getInstance();
	//variabel yang merefers ke Firebase Database
	private DatabaseReference mDatabaseReference;
	private FirebaseDatabase mFirebaseInstance;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


		
		mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		
		
		FirebaseApp.initializeApp(this);
		// mengambil referensi ke Firebase Database
		mFirebaseInstance = FirebaseDatabase.getInstance();
		mDatabaseReference = mFirebaseInstance.getReference("Quiz");
		mDatabaseReference.child("Questions").addValueEventListener(new ValueEventListener(){
			@Override
			public void onDataChange(DataSnapshot dataSnapshot){
				
				daftarQuiz = new ArrayList<>();
				for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()){
					Quiz quiz = mDataSnapshot.getValue(Quiz.class);
					quiz.setKey(mDataSnapshot.getKey());
					daftarQuiz.add(quiz);
				}
				//set adapter RecyclerView
				mAdapter = new com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.AdapterAbsenRecyclerView(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this, daftarQuiz);
				mRecyclerView.setAdapter(mAdapter);
			}
			
			@Override
			public void onCancelled(DatabaseError databaseError){
				// TODO: Implement this method
				Toast.makeText(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this, databaseError.getDetails()+" "+databaseError.getMessage(), Toast.LENGTH_LONG).show();
			}
				
		});
		
		
		//FAB (FloatingActionButton) tambah Absen
		mFloatingActionButton = (FloatingActionButton)findViewById(R.id.tambah_quiz);
		mFloatingActionButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				//tambah Absen
				dialogTambahabsen();
			}
		});



    }
	
	
	
	
	/* method ketika data di klik
	*/
	@Override
	public void onDataClick(final Quiz quiz, int position){
		//aksi ketika data di klik
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pilih Aksi");
		
		builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int id){
				dialogUpdateAbsen(quiz);
			}
		});
		builder.setNegativeButton("HAPUS", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int id){
				hapusDataAbsen(quiz);
			}
		});
		builder.setNeutralButton("BATAL", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int id){
				dialog.dismiss();
			}
		});
		
		Dialog dialog = builder.create();
		dialog.show();
	}


	
	//setup android toolbar




	//dialog tambah Absen / alert dialog
	private void dialogTambahabsen(){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Tambah Data absen");
		View view = getLayoutInflater().inflate(R.layout.layout_tambah_quiz, null);

		mEditSoal = (EditText)view.findViewById(R.id.soal_quiztxt);
		mEditOp1 = (EditText)view.findViewById(R.id.option1txt);
		mEditOp2 = (EditText)view.findViewById(R.id.option2txt);
		mEditOp3 = (EditText)view.findViewById(R.id.option3txt);
		mEditOp4= (EditText)view.findViewById(R.id.option4txt);
		mEditJawaban = (EditText)view.findViewById(R.id.jawaban_quiztxt);

		builder.setView(view);

		//button simpan dosen / submit absen
		builder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int id){

					String soalQuiz = mEditSoal.getText().toString();
					String op1 = mEditOp1.getText().toString();
					String op2 = mEditOp2.getText().toString();
					String op3 = mEditOp3.getText().toString();
					String op4 = mEditOp4.getText().toString();
					String jawabanQuiz = mEditJawaban.getText().toString();


					if(!soalQuiz.isEmpty() && !op1.isEmpty() && !op2.isEmpty() && !op3.isEmpty() && !op4.isEmpty()&& !jawabanQuiz.isEmpty()){
						submitDataAbsen(new Quiz(soalQuiz, op1, op2,op3, op4, jawabanQuiz));
					}
					else {
						Toast.makeText(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this, "Data harus di isi!", Toast.LENGTH_LONG).show();
					}
				}
			});

		//button kembali / batal
		builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int id){
					dialog.dismiss();
				}
			});
		Dialog dialog = builder.create();
		dialog.show();
	}


	//dialog update Absen / update data Absen
	private void dialogUpdateAbsen(final Quiz quiz){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Edit Data Absen");
		View view = getLayoutInflater().inflate(R.layout.layout_edit_quiz, null);

		mEditSoal = (EditText)view.findViewById(R.id.soal_quiztxt);
		mEditOp1 = (EditText)view.findViewById(R.id.option1txt);
		mEditOp2 = (EditText)view.findViewById(R.id.option2txt);
		mEditOp3 = (EditText)view.findViewById(R.id.option3txt);
		mEditOp4= (EditText)view.findViewById(R.id.option4txt);
		mEditJawaban = (EditText)view.findViewById(R.id.jawaban_quiztxt);



		mEditSoal.setText(quiz.getQuestion());
		mEditOp1.setText(quiz.getOption1());
		mEditOp2.setText(quiz.getOption2());
		mEditOp3.setText(quiz.getOption3());
		mEditOp4.setText(quiz.getOption4());
		mEditJawaban.setText(quiz.getAnswer());
		builder.setView(view);
		
		//final absen mabsen = (absen)getIntent().getSerializableExtra("
		if (quiz != null){
			builder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int id){
					quiz.setQuestion(mEditSoal.getText().toString());
					quiz.setOption1(mEditOp1.getText().toString());
					quiz.setOption2(mEditOp2.getText().toString());
					quiz.setOption3(mEditOp3.getText().toString());
					quiz.setOption4(mEditOp4.getText().toString());
					quiz.setAnswer(mEditJawaban.getText().toString());
					updateDataAbsen(quiz);
				}
			});
		}
		builder.setNegativeButton("BATAL", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int id){
				dialog.dismiss();
			}
		});
		Dialog dialog = builder.create();
		dialog.show();

	}
	
	
	/**
	 * submit data absen
	 * ini adalah kode yang digunakan untuk mengirimkan data ke Firebase Realtime Database
	 * set onSuccessListener yang berisi kode yang akan dijalankan
	 * ketika data berhasil ditambahkan
	 */
	private void submitDataAbsen(Quiz quiz){
		mDatabaseReference.child("Questions").push().setValue(quiz).addOnSuccessListener(this, new OnSuccessListener<Void>(){
			@Override
			public void onSuccess(Void mVoid){
				Toast.makeText(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this, "Data Absen berhasil di simpan !", Toast.LENGTH_LONG).show();
			}
		});
	}
	
	/**
	 * update/edit data Absen
	 * ini adalah kode yang digunakan untuk mengirimkan data ke Firebase Realtime Database
	 * set onSuccessListener yang berisi kode yang akan dijalankan
	 * ketika data berhasil ditambahkan
	 */
	private void updateDataAbsen(Quiz quiz){
		mDatabaseReference.child("Questions").child(quiz.getKey()).setValue(quiz).addOnSuccessListener(new OnSuccessListener<Void>(){
			@Override
			public void onSuccess(Void mVoid){
				Toast.makeText(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this, "Data berhasil di update !", Toast.LENGTH_LONG).show();
			}
		});
	}
	/**
	 * hapus data absen
	 * ini kode yang digunakan untuk menghapus data yang ada di Firebase Realtime Database
	 * set onSuccessListener yang berisi kode yang akan dijalankan
	 * ketika data berhasil dihapus
	 */
	private void hapusDataAbsen(Quiz quiz){
		if(mDatabaseReference != null){
			mDatabaseReference.child("Questions").child(quiz.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>(){
				@Override
				public void onSuccess(Void mVoid){
					Toast.makeText(com.muhammadfadhlidzilikram.apps_quiz_firebase.admin.MainActivity.this,"Data berhasil di hapus !", Toast.LENGTH_LONG).show();
				}
			});
		}
	}
}
