package com.muhammadfadhlidzilikram.apps_quiz_firebase.admin;


import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.muhammadfadhlidzilikram.apps_quiz_firebase.R;

public class ItemAbsenViewHolder extends RecyclerView.ViewHolder
{
	public TextView soalQuiz;
	public TextView jawabanQuiz;

	public View view;
	
	public ItemAbsenViewHolder(View view){
		super(view);
		
		soalQuiz = (TextView)view.findViewById(R.id.soal_quiztxt);
		jawabanQuiz = (TextView)view.findViewById(R.id.jawaban_quiztxt);

		this.view = view;
	}
}
