package com.fadhlidzilikampohan.amoniasensor.Fragment;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.fadhlidzilikampohan.amoniasensor.R;
import com.fadhlidzilikampohan.amoniasensor.notif.RequestHandler;
import com.fadhlidzilikampohan.amoniasensor.notif.konfigurasi;

import java.util.HashMap;

public class conto extends Fragment {
    private TextView editTextToken;
    private EditText editTextNama;
    private EditText editTextEmail;

    private Button buttonAdd;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_PHONE_STATE}, 1);
        }

        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();

        String token = FirebaseInstanceId.getInstance().getToken();
        editTextToken = view.findViewById(R.id.editTexttoken);
        editTextToken.setText(getResources().getString(R.string.token, token));

        //Inisialisasi dari View

        editTextNama = view.findViewById(R.id.editTextnama);
        editTextEmail = view.findViewById(R.id.editTextemail);

        buttonAdd = view.findViewById(R.id.buttonAdd);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_conto, container,false);
        buttonAdd = (Button) view.findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == buttonAdd){
                    addEmployee();
                }
            }

            private void addEmployee() {
                final String token1 = editTextToken.getText().toString().trim();
                final String nama1 = editTextNama.getText().toString().trim();
                final String email1 = editTextEmail.getText().toString().trim();

                class AddEmployee extends AsyncTask<Void, Void, String> {

                    ProgressDialog loading;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loading = ProgressDialog.show(getActivity(),"Menambahkan...","Tunggu...",false,false);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        loading.dismiss();
                        Toast.makeText(getActivity(),s, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    protected String doInBackground(Void... v) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(konfigurasi.KEY_EMP_TOKEN,token1);
                        params.put(konfigurasi.KEY_EMP_NAMA,nama1);
                        params.put(konfigurasi.KEY_EMP_EMAIL,email1);

                        RequestHandler rh = new RequestHandler();
                        String res = rh.sendPostRequest(konfigurasi.URL_ADD, params);
                        return res;
                    }
                }

                AddEmployee ae = new AddEmployee();
                ae.execute();
            }
        });
        return view;
    }



}