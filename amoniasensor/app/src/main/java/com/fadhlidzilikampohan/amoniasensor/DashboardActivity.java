package com.fadhlidzilikampohan.amoniasensor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DashboardActivity extends Activity {
    private SessionHandler session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        session = new SessionHandler(getApplicationContext());
        User user = session.getUserDetails();
        TextView welcomeText = findViewById(R.id.welcomeText);

        welcomeText.setText("Selamat Datang "+user.getFullName()+", Kamu akan keluar dari aplikasi pada "+user.getSessionExpiryDate()+" secara otomatis akan dialihkan ke menu login ");

        Button logoutBtn = findViewById(R.id.btnLogout);
        Button masukBtn = findViewById(R.id.btnmasuk);


        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logoutUser();
                Intent i = new Intent(com.fadhlidzilikampohan.amoniasensor.DashboardActivity.this, LoginActivity.class);
                startActivity(i);
                finish();

            }
        });

        masukBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.isLoggedIn();
                Intent i = new Intent(com.fadhlidzilikampohan.amoniasensor.DashboardActivity.this, com.fadhlidzilikampohan.amoniasensor.awal.splasscren.class);
                startActivity(i);
                finish();


            }
        });
    }
}
