package com.fadhlidzilikampohan.amoniasensor.awal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.fadhlidzilikampohan.amoniasensor.MainActivity;
import com.fadhlidzilikampohan.amoniasensor.MainActivity1;
import com.fadhlidzilikampohan.amoniasensor.R;

public class splasscren extends Activity {

    private int waktu_loading = 1500;

    //4000=4 detik
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splasscren);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//setelah loading maka akan langsung berpindah ke home activity
                Intent home = new Intent(splasscren.this,
                        MainActivity1.class);
                startActivity(home);
                finish();
            }
        }, waktu_loading);
    }
}
