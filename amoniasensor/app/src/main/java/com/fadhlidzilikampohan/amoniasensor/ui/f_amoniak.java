package com.fadhlidzilikampohan.amoniasensor.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.fadhlidzilikampohan.amoniasensor.Fragment.amoniak;
import com.fadhlidzilikampohan.amoniasensor.Fragment.chart_amoniak;
import com.fadhlidzilikampohan.amoniasensor.R;

;

public class f_amoniak extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_amoniak, container, false);
        return view;

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            amoniak f_amoniak = new amoniak();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_amoniak, f_amoniak)
                    .commit();
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.nav_view_amoniak);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data:
                amoniak ff_amoniak = new amoniak();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_amoniak, ff_amoniak)
                        .commit();
                return true;

            case R.id.navigation_chart:
                chart_amoniak c_amoniak = new chart_amoniak();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_amoniak, c_amoniak)
                        .commit();
                return true;

        }
        return false;
    }

}
