package com.fadlidzilikram.pusri_intro_slider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.HashMap;
import androidx.appcompat.app.AppCompatActivity;


public class data_aduan_kerusakan extends AppCompatActivity implements View.OnClickListener{

    //Dibawah ini merupakan perintah untuk mendefinikan View
    private EditText editTextTipex;
    private EditText editTextUnitkerjax;
    private EditText editTextPenghubungx;
    private EditText editTextNotlpx;
    private EditText editTextEmailpx;
    private EditText editTextEmailax;
    private EditText editTextDeskripsix;
    private EditText editTextAksix;
    private EditText editTextTanggalx;


    private Button buttonAdd;
    private Button buttonView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_aduan_kerusakan);

        //Inisialisasi dari View
        editTextTipex= (EditText) findViewById(R.id.editTextTipe);
        editTextUnitkerjax = (EditText) findViewById(R.id.editTextUnitkerja);
        editTextPenghubungx = (EditText) findViewById(R.id.editTextpenghubung);
        editTextNotlpx = (EditText) findViewById(R.id.editTextNotlp);
        editTextEmailpx = (EditText) findViewById(R.id.editTextEmailp);
        editTextEmailax = (EditText) findViewById(R.id.editTextEmaila);
        editTextDeskripsix = (EditText) findViewById(R.id.editTextDeskripsi);
        editTextAksix = (EditText) findViewById(R.id.editTextAksi);
        editTextTanggalx = (EditText) findViewById(R.id.editTextTanggal);


        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonView = (Button) findViewById(R.id.buttonView);

        //Setting listeners to button
        buttonAdd.setOnClickListener(this);
        buttonView.setOnClickListener(this);
    }


    //Dibawah ini merupakan perintah untuk Menambahkan Aduan (CREATE)
    private void addEmployee(){

        final String tipen = editTextTipex.getText().toString().trim();
        final String unitkerjan = editTextUnitkerjax.getText().toString().trim();
        final String penghubungn = editTextPenghubungx.getText().toString().trim();
        final String notelpn = editTextNotlpx.getText().toString().trim();
        final String emailpn = editTextEmailpx.getText().toString().trim();
        final String emailan = editTextEmailax.getText().toString().trim();
        final String deskripsin = editTextDeskripsix.getText().toString().trim();
        final String aksin = editTextAksix.getText().toString().trim();
        final String tanggaln = editTextTanggalx.getText().toString().trim();

        class AddEmployee extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(data_aduan_kerusakan.this,"Menambahkan...","Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(data_aduan_kerusakan.this,s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String, String> params = new HashMap<>();
                params.put(konfigurasi.KEY_EMP_TIPE,tipen);
                params.put(konfigurasi.KEY_EMP_UNITKERJA,unitkerjan);
                params.put(konfigurasi.KEY_EMP_PENGHUBUNG,penghubungn);
                params.put(konfigurasi.KEY_EMP_NOTLP,notelpn);
                params.put(konfigurasi.KEY_EMP_EMAILP,emailpn);
                params.put(konfigurasi.KEY_EMP_EMAILA,emailan);
                params.put(konfigurasi.KEY_EMP_DESKRIPSI,deskripsin);
                params.put(konfigurasi.KEY_EMP_AKSI,aksin);
                params.put(konfigurasi.KEY_EMP_TANGGAL,tanggaln);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(konfigurasi.URL_ADD, params);
                return res;
            }
        }

        AddEmployee ae = new AddEmployee();
        ae.execute();
    }

    @Override
    public void onClick(View v) {
        if(v == buttonAdd){
            addEmployee();
        }

        if(v == buttonView){
            startActivity(new Intent(this,data_tracking.class));
        }
    }
}

