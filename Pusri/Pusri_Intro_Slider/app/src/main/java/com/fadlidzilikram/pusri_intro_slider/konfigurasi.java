package com.fadlidzilikram.pusri_intro_slider;



public class konfigurasi {

    //Dibawah ini merupakan Pengalamatan dimana Lokasi Skrip CRUD PHP disimpan
    //Pada tutorial Kali ini, karena kita membuat localhost maka alamatnya tertuju ke IP komputer
    //dimana File PHP tersebut berada
    //PENTING! JANGAN LUPA GANTI IP SESUAI DENGAN IP KOMPUTER DIMANA DATA PHP BERADA

    public static final String URL_ADD="https://passnyacoba123tandaseru.000webhostapp.com/aduanperbaikan/tambahPgw.php";
    public static final String URL_GET_ALL = "https://passnyacoba123tandaseru.000webhostapp.com/aduanperbaikan/tampilSemuaPgw.php";
    public static final String URL_GET_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/aduanperbaikan/tampilPgw.php?id=";
    public static final String URL_UPDATE_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/aduanperbaikan/updatePgw.php";
    public static final String URL_DELETE_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/aduanperbaikan/hapusPgw.php?id=";

    //Dibawah ini merupakan Kunci yang akan digunakan untuk mengirim permintaan ke Skrip PHP
    //yang diisi variabel bukan databasenya
    public static final String KEY_EMP_NOTIKET = "notiketz";
    public static final String KEY_EMP_TIPE = "tipez";
    public static final String KEY_EMP_UNITKERJA = "unitkerjaz";
    public static final String KEY_EMP_PENGHUBUNG = "penghubungz";
    public static final String KEY_EMP_NOTLP = "notlpz";
    public static final String KEY_EMP_EMAILP = "emailpz";
    public static final String KEY_EMP_EMAILA = "emailaz";
    public static final String KEY_EMP_DESKRIPSI = "deskripsiz"; //deskripsix itu variabel untuk deskripsi di database
    public static final String KEY_EMP_AKSI = "aksiz";
    public static final String KEY_EMP_TANGGAL = "tanggalz";
   

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NOTIKET = "notiketz";
    public static final String TAG_TIPEX = "tipez";
    public static final String TAG_UNITKERJA = "unitkerjaz";
    public static final String TAG_PENGHUBUNG = "penghubungz";
    public static final String TAG_NOTLP = "notlpz";
    public static final String TAG_EMAILPX = "emailpz";
    public static final String TAG_EMAILAX = "emailaz";
    public static final String TAG_DESKRIPSIX = "deskripsiz";
    public static final String TAG_AKSI = "aksiz";
    public static final String TAG_TANGGAL = "tanggalz";


    //ID karyawan
    //emp itu singkatan dari Employee
    public static final String EMP_NOTIKET = "emp_notiketx";
}
