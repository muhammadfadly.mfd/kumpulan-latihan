package com.fadlidzilikram.pusri_intro_slider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.fadlidzilikram.pusri_intro_slider.awal.intro;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void masuk(View view) {
        Intent intent = new Intent(MainActivity.this, data_tracking.class);
        startActivity(intent);
    }

    public void ulan(View view) {
        Intent intent = new Intent(MainActivity.this, intro.class);
        startActivity(intent);
    }
}
