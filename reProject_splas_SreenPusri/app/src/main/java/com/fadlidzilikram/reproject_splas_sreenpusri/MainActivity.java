 package com.fadlidzilikram.reproject_splas_sreenpusri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Splash.Builder splash = new Splash.Builder(this, getSupportActionBar());
        splash.perform();
    }
}
