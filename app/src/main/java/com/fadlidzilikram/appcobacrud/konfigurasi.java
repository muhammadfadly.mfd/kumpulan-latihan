package com.fadlidzilikram.appcobacrud;

/**
 * Created by muhammadyusuf on 01/19/2017.
 * kodingindonesia
 */

public class konfigurasi {

    //Dibawah ini merupakan Pengalamatan dimana Lokasi Skrip CRUD PHP disimpan
    //Pada tutorial Kali ini, karena kita membuat localhost maka alamatnya tertuju ke IP komputer
    //dimana File PHP tersebut berada
    //PENTING! JANGAN LUPA GANTI IP SESUAI DENGAN IP KOMPUTER DIMANA DATA PHP BERADA

    public static final String URL_ADD ="https://passnyacoba123tandaseru.000webhostapp.com/coba/tambahPgw.php";
    public static final String URL_GET_ALL = "https://passnyacoba123tandaseru.000webhostapp.com/coba/tampilSemuaPgw.php";
    public static final String URL_GET_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/coba/tampilPgw.php?id=";
    public static final String URL_UPDATE_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/coba/updatePgw.php";
    public static final String URL_DELETE_EMP = "https://passnyacoba123tandaseru.000webhostapp.com/coba/hapusPgw.php?id=";

    //Dibawah ini merupakan Kunci yang akan digunakan untuk mengirim permintaan ke Skrip PHP
    public static final String KEY_EMP_ID = "id";
    public static final String KEY_EMP_NAMA = "names";
    public static final String KEY_EMP_ALAMAT = "places"; //desg itu variabel untuk posisi
    public static final String KEY_EMP_PEKERJAAN = "jobs"; //salary itu variabel untuk gajih

    //JSON Tags
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_ID = "id";
    public static final String TAG_NAMA = "names";
    public static final String TAG_ALAMAT = "places";
    public static final String TAG_PEKERJAAN = "jobs";

    //ID karyawan
    //emp itu singkatan dari Employee
    public static final String EMP_ID = "emp_id";
}
