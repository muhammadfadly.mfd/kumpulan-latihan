package com.muhammadfadhlidzilikram.apps_quiz3;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DifficultyFragment extends Fragment {

    Button easyBtn;
    Button mediumBtn;
    public static boolean isEasy;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_difficulty, container, false);

        ((MainActivity)getActivity()).hideBottomNav();

        easyBtn = rootView.findViewById(R.id.easyBtn);
        mediumBtn = rootView.findViewById(R.id.mediumBtn);

        easyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment();
                isEasy = true;
            }
        });

        mediumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment();
                isEasy = false;
            }
        });


        return rootView;
    }

    public void replaceFragment () {
        Fragment fragment = new MultipleChoiceFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
