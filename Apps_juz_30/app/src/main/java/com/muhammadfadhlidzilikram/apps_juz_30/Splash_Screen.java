package com.muhammadfadhlidzilikram.apps_juz_30;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class Splash_Screen extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext() ,Menu.class );
                Splash_Screen.this.startActivity(i);
                Splash_Screen.this.finish();
            }
        },SPLASH_DISPLAY_LENGTH);


    }
}
