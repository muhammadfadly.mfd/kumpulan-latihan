package com.muhammadfadhlidzilikram.apps_juz_30;

import android.content.Intent;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class Tips_Tips_Menghafal extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        Intent a = new Intent(getApplicationContext(),Menu.class);
        startActivity(a);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips__tips__menghafal);
    }
}
