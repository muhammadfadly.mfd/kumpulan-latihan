package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model;

import java.util.List;

public class ResponseModel {
    private int kode;
    private String pesan;
    private List<com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model.DataModel> data;

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model.DataModel> getData() {
        return data;
    }

    public void setData(List<com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model.DataModel> data) {
        this.data = data;
    }
}
