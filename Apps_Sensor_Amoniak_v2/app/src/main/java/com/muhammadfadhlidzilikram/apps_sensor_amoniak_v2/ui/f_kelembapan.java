package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.amoniak;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_amoniak;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_kelembapan;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.kelembapan;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.R;

import static androidx.navigation.Navigation.findNavController;


public class f_kelembapan extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_kelembapan, container, false);


        return view;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data1:
                kelembapan f_kelembapan = new kelembapan();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, f_kelembapan)
                        .commit();
                return true;

            case R.id.navigation_chart1:
                chart_kelembapan c_kelembapan = new chart_kelembapan();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, c_kelembapan)
                        .commit();
                return true;

        }
        return false;
    }

}
