package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.ui;


import android.os.Bundle;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_suhu;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.suhu;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.R;


public class f_suhu extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_suhu, container, false);

        return view;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data2:
                suhu f_suhu = new suhu();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, f_suhu)
                        .commit();
                return true;

            case R.id.navigation_chart2:
                chart_suhu c_suhu = new chart_suhu();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, c_suhu)
                        .commit();
                return true;

        }
        return false;
    }
}


