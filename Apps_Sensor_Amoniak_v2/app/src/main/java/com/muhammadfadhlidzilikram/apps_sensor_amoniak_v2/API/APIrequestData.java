package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.API;

import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIrequestData {
    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();

}
