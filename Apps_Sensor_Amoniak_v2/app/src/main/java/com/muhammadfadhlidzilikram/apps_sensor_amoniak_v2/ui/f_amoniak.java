package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.ui;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.ActivityNavigator;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.amoniak;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_amoniak;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_suhu;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.R;

import static androidx.navigation.Navigation.findNavController;

public class f_amoniak extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_amoniak, container, false);
        return view;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data:
                amoniak f_amoniak = new amoniak();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, f_amoniak)
                        .commit();
                return true;

            case R.id.navigation_chart:
                chart_amoniak c_amoniak = new chart_amoniak();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_view_amoniak, c_amoniak)
                        .commit();
                return true;

        }
        return false;
    }
}
