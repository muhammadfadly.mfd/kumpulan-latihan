package com.fadlidzilikram.app_pet_crud.awal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.fadlidzilikram.app_pet_crud.MainActivity;
import com.fadlidzilikram.app_pet_crud.R;


import androidx.appcompat.app.AppCompatActivity;

public class splasscren extends Activity {

    private int waktu_loading=1500;
    //4000=4 detik
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splasscren);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//setelah loading maka akan langsung berpindah ke home activity
                Intent home=new Intent(splasscren.this,
                        MainActivity.class);
                startActivity(home);
                finish();}
        },waktu_loading);
    }
}
