package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.ui;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.chart_suhu;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Fragment.suhu;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.R;



public class f_suhu extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_suhu, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            suhu ff_suhu = new suhu();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_suhu, ff_suhu)
                    .commit();
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.nav_view_suhu);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data2:
                suhu ff_suhu = new suhu();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_suhu, ff_suhu)
                        .commit();
                return true;

            case R.id.navigation_chart2:
                chart_suhu c_suhu = new chart_suhu();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_suhu, c_suhu)
                        .commit();
                return true;

        }
        return false;
    }

}

