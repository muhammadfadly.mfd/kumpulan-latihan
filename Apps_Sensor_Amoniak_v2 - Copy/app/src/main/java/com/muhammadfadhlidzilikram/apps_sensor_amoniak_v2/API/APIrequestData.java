package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.API;

import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIrequestData {
    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();

    @GET("retrieve_kelembapan.php")
    Call<ResponseModel> ardRetrieveData2();

    @GET("retrieve_suhu.php")
    Call<ResponseModel> ardRetrieveData3();

}
