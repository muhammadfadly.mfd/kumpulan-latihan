package com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.awal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.R;
import com.muhammadfadhlidzilikram.apps_sensor_amoniak_v2.MainActivity;

public class splasscren extends Activity {

    private int waktu_loading = 1500;

    //4000=4 detik
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splasscren);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//setelah loading maka akan langsung berpindah ke home activity
                Intent home = new Intent(splasscren.this,
                        MainActivity.class);
                startActivity(home);
                finish();
            }
        }, waktu_loading);
    }
}
