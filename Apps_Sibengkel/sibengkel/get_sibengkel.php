<?php 

header("Content-Type: application/json; charset=UTF-8");

require_once 'connect.php';

$query = "SELECT * FROM tb_sibengkel ORDER BY id DESC ";
$result = mysqli_query($conn, $query);
$response = array();

$server_name = $_SERVER['SERVER_ADDR'];

while( $row = mysqli_fetch_assoc($result) ){

    array_push($response, 
    array(
        'id'        =>$row['id'], 
        'nama'      =>$row['nama'], 
        'dept'   =>$row['dept'],
        'kerusakan'     =>$row['kerusakan'],
        'status'    =>$row['status'],
        'tgl'     =>date('d M Y', strtotime($row['tgl'])),
        'picture'   =>"http://$server_name" . $row['picture'],
        'cek'      =>$row['cek']) 
    );
}

echo json_encode($response);

mysqli_close($conn);

?>