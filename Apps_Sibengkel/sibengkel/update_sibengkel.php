<?php 

header("Content-Type: application/json; charset=UTF-8");

require_once 'connect.php';

$key = $_POST['key'];

if ( $key == "update" ){

    $id         = $_POST['id'];
    $nama       = $_POST['nama'];
    $dept    = $_POST['dept'];
    $kerusakan      = $_POST['kerusakan'];
    $status     = $_POST['status'];
    $tgl      = $_POST['tgl'];
    $picture    = $_POST['picture'];

    $tgl =  date('Y-m-d', strtotime($tgl));

    $query = "UPDATE tb_sibengkel SET 
    nama='$nama', 
    dept='$dept', 
    kerusakan='$kerusakan',
    status='$status',
    tgl='$tgl' 
    WHERE id='$id' ";

        if ( mysqli_query($conn, $query) ){

            if ($picture == null) {

                $result["value"] = "1";
                $result["message"] = "Success";
    
                echo json_encode($result);
                mysqli_close($conn);

            } else {

                $path = "sibengkel_picture/$id.jpeg";
                $finalPath = "/sibengkel/".$path;

                $insert_picture = "UPDATE tb_sibengkel SET picture='$finalPath' WHERE id='$id' ";
            
                if (mysqli_query($conn, $insert_picture)) {
            
                    if ( file_put_contents( $path, base64_decode($picture) ) ) {
                        
                        $result["value"] = "1";
                        $result["message"] = "Success!";
            
                        echo json_encode($result);
                        mysqli_close($conn);
            
                    } else {
                        
                        $response["value"] = "0";
                        $response["message"] = "Error! ".mysqli_error($conn);
                        echo json_encode($response);

                        mysqli_close($conn);
                    }

                }
            }

        } 
        else {
            $response["value"] = "0";
            $response["message"] = "Error! ".mysqli_error($conn);
            echo json_encode($response);

            mysqli_close($conn);
        }
}

?>