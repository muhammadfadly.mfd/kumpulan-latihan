package com.muhammadfadhlidzilikram.tabdanbottom;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.muhammadfadhlidzilikram.tabdanbottom.ui.home.HomeFragment;
import com.muhammadfadhlidzilikram.tabdanbottom.ui.notifications.NotificationsFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    LinearLayout layout_tab;

    private BottomNavigationView.OnNavigationItemSelectedListener monNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                load_fragment_bottom(new HomeFragment());
                layout_tab.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                return true;

            case R.id.navigation_dashboard:
                layout_tab.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                return true;

            case R.id.navigation_notifications:
                load_fragment_bottom(new NotificationsFragment());
                layout_tab.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                return true;

        }
        return false;
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        navView.setOnNavigationItemReselectedListener((BottomNavigationView.OnNavigationItemReselectedListener) monNavigationItemSelectedListener);

        layout_tab = (LinearLayout) findViewById(R.id.layout_tab);
        frameLayout = (FrameLayout) findViewById(R.id.layout_frame);

        load_fragment_bottom(new HomeFragment());


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
    }

    Boolean load_fragment_bottom(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.layout_frame, fragment).commit();
            return true;
        }
        return false;
    }

}