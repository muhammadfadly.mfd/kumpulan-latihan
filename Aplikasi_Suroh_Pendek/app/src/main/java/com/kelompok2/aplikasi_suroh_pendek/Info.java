package com.kelompok2.aplikasi_suroh_pendek;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class Info extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        Intent a = new Intent(getApplicationContext(),Menu.class);
        startActivity(a);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }
}
