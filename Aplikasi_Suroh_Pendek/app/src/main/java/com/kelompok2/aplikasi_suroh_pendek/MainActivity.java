package com.kelompok2.aplikasi_suroh_pendek;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    @Override
    public void onBackPressed() {
        Intent b = new Intent(getApplicationContext(),Menu.class);
        startActivity(b);
        finish();
    }

    ListView listView;

    String nama_surat [] = {
            "Al-qari'ah (Hari Kiamat)","At-takatsur (Bermegah-Megahan)",
            "Al-ashr (Masa/Waktu)","Al-humazah (Pengumpat)","Al-fil (Gajah)",
            "Al-quraisy (Kaum Quraisy)","Al-maun (Barang-Barang Yang Berguna)",
            "Al-kautsar (Nikmat Yang Berlimpah)","Al-kafirun (Orang-Orang Kafir)",
            "An-nasr (Pertolongan)","Al-lahab (Gejolak Api)","Al-ikhlas ( Orang-Orang Ikhlas)",
            "Al-falaq (Waktu Subuh)","An-nas (Manusia)"
    };

    String isi_surat [] = {
          String.valueOf(R.string.alqoriah),String.valueOf(R.string.attakatsur),
            String.valueOf(R.string.alasr),String.valueOf(R.string.alhumazah),String.valueOf(R.string.alfil),String.valueOf(R.string.alquraisy),
            String.valueOf(R.string.almaun),String.valueOf(R.string.alkautsar),String.valueOf(R.string.alkafirun),String.valueOf(R.string.annashr),
            String.valueOf(R.string.allahab),String.valueOf(R.string.alikhlas),String.valueOf(R.string.alfalaq),String.valueOf(R.string.annas)
    };
    int suara_surat [] = {
                       R.raw.al_qariah,R.raw.at_takatsur,R.raw.al_asr,R.raw.al_humazah,R.raw.al_fil,R.raw.al_quraisy,R.raw.al_maun,R.raw.al_kautsar,
            R.raw.al_kafirun,R.raw.an_nashr,R.raw.al_lahab,R.raw.al_ikhlas,R.raw.al_falaq,R.raw.an_nas
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listjuzamma);

        Adapter adapterListJuzAmma = new Adapter(this,nama_surat,isi_surat, suara_surat);
        listView.setAdapter(adapterListJuzAmma);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent a = new Intent(getApplicationContext(), Detail_Activity.class);
                a.putExtra("nama", nama_surat[position]);
                int s = suara_surat[position];
                a.putExtra("suara", s);
                a.putExtra("isi",isi_surat[position]);
                startActivity(a);
                finish();

            }
        });
    }
}
