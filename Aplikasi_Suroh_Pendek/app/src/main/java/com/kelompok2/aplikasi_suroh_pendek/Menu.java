package com.kelompok2.aplikasi_suroh_pendek;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class Menu extends AppCompatActivity {
    Dialog mydialog;
    TextView txtjudul;
    TextView txtsemangat;
    ImageView imgquran;
    ImageView imghafidz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
       ImageView imgquran = (ImageView) findViewById(R.id.imgsurah);
        ImageView imgtips = (ImageView) findViewById(R.id.imgtips);
        mydialog=new Dialog(this);

       imgquran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplicationContext(), com.kelompok2.aplikasi_suroh_pendek.MainActivity.class );
                startActivity(a);
                finish();
            }
        });

        imgtips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtclose;
                Button btnFollow;
                mydialog.setContentView(R.layout.activity_info);
                txtclose=(TextView)mydialog.findViewById(R.id.txtclose);
                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mydialog.dismiss();
                    }
                });
                mydialog.getWindow().setBackgroundDrawable(new
                        ColorDrawable(Color.TRANSPARENT));
                mydialog.show();
            }
        });

    }
}
