 package com.fadlidzilikram.project_simpl_quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

 public class MainActivity extends AppCompatActivity {

    private QuestionLibrary mQuestionLibrary = new QuestionLibrary();
    private TextView mScoreView;
    private TextView mQuestionView;
    private Button mButtonChoice1;
    private Button mButtonChoice2;
    private Button mButtonChoice3;
    private String mAnswer;
    private int mScore = 0;
    private int mQuestionNumber = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mScoreView = (TextView) findViewById(R.id.score);
        mQuestionView = (TextView) findViewById(R.id.question);
        mButtonChoice1 = (Button) findViewById(R.id.choice1);
        mButtonChoice2 = (Button) findViewById(R.id.choice2);
        mButtonChoice3 = (Button) findViewById(R.id.choice3);
        updateQuestion();
//Listener tombol pilihan 1
        mButtonChoice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//logika program
                if (mButtonChoice1.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
//toas
                    Toast.makeText(MainActivity.this, "BENAR",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "SALAH",
                            Toast.LENGTH_SHORT).show();
                    updateQuestion();
                }
            }
        });
//Listener tombol pilihan 2
        mButtonChoice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//logika program
                if (mButtonChoice2.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
//toas
                    Toast.makeText(MainActivity.this, "BENAR",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "SALAH",
                            Toast.LENGTH_SHORT).show();
                    updateQuestion();
                }
            }
        });
//Listener tombol pilihan 3
        mButtonChoice3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//logika program
                if (mButtonChoice3.getText() == mAnswer) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
//toas
                    Toast.makeText(MainActivity.this, "BENAR",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "SALAH",
                            Toast.LENGTH_SHORT).show();
                    updateQuestion();
                }
            }
        });
    }
    private void updateQuestion(){
        if (mQuestionNumber<mQuestionLibrary.getLength()){
            mQuestionView.setText(mQuestionLibrary.getQuestion(mQuestionNumber));
            mButtonChoice1.setText(mQuestionLibrary.getChoice1(mQuestionNumber));
            mButtonChoice2.setText(mQuestionLibrary.getChoice2(mQuestionNumber));
            mButtonChoice3.setText(mQuestionLibrary.getChoice3(mQuestionNumber));
            mAnswer=mQuestionLibrary.getCorrectAnswer(mQuestionNumber);
            mQuestionNumber++;
        }else {
            gameOver();
        }}
    private void updateScore(int point){
        mScoreView.setText(""+mScore);
    }
    private void gameOver(){
        AlertDialog.Builder alertDialogBuilder=new
                AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder
                .setMessage(" Skor Kamu " + mScore + " poin")
                .setCancelable(false)
                .setPositiveButton("MAIN LAGI",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                startActivity(new
                                        Intent(getApplicationContext(), MainActivity.class));
                            }
                        })
                .setNegativeButton("KELUAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finish();
                            }
                        });
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
    }
}
