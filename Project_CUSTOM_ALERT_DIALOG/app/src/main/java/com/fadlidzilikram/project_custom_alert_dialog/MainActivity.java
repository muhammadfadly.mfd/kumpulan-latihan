package com.fadlidzilikram.project_custom_alert_dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showDialog(View view) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Tentang");
        alertDialog.setMessage("Aplikasi ini merupakan contoh dari custom alert dialog yang dibuat menggunakan android studio  ");
        // Alert dialog button
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YA",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Alert dialog action goes here
                        // onClick button code here
                        dialog.dismiss();// use dismiss to cancel alert dialog
                    }
                });
        alertDialog.show();
    }

    public void showDialog2(View view) {
        AlertDialog.Builder BackAlertDialog = new AlertDialog.Builder(MainActivity.this);

        BackAlertDialog.setTitle("Keluar");

        BackAlertDialog.setMessage("Anda yakin ingin keluar dari Aplikasi ini  ?");


        BackAlertDialog.setPositiveButton("TIDAK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        //Cancel alert dialog box .
                        dialog.cancel();
                    }
                });

        BackAlertDialog.setNegativeButton("YA",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        //Exit from activity.
                        finish();
                    }
                });

        BackAlertDialog.show();

        return;
    }

}