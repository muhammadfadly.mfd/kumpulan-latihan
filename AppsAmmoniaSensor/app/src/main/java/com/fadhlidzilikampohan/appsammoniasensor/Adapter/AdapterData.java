package com.fadhlidzilikampohan.appsammoniasensor.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.fadhlidzilikampohan.appsammoniasensor.Model.DataModel;
import com.fadhlidzilikampohan.appsammoniasensor.R;

import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData>{
    private Context ctx;
    private List<DataModel> listData;

    public AdapterData(Context ctx, List<DataModel> listData){
        this.ctx = ctx;
        this.listData = listData;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        HolderData holder = new HolderData(layout);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DataModel dm = listData.get(position);

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getRandomColor();

        holder.itemView.setBackgroundColor(color);
        holder.tvStatus.setText(String.valueOf(dm.getStatus()));
        holder.tvId.setText(String.valueOf(dm.getId()));
        holder.tvNilai.setText(dm.getNilai());
        holder.tvWaktu.setText(dm.getWaktu());



    }



    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder{
        TextView tvId, tvStatus, tvNilai, tvWaktu;

        public HolderData(@NonNull View itemView) {
            super(itemView);

            tvStatus = itemView.findViewById(R.id.tv_status);
            tvId = itemView.findViewById(R.id.tv_id);

            tvNilai = itemView.findViewById(R.id.tv_nilai);
            tvWaktu = itemView.findViewById(R.id.tv_waktu);

        }
    }
}
