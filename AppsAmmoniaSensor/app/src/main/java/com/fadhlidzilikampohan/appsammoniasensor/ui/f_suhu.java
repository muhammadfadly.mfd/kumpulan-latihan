package com.fadhlidzilikampohan.appsammoniasensor.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.fadhlidzilikampohan.appsammoniasensor.Fragment.chart_suhu;
import com.fadhlidzilikampohan.appsammoniasensor.Fragment.suhu;
import com.fadhlidzilikampohan.appsammoniasensor.R;


public class f_suhu extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_f_suhu, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            suhu ff_suhu = new suhu();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_suhu, ff_suhu)
                    .commit();
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.nav_view_suhu);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.navigation_data2:
                suhu ff_suhu = new suhu();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_suhu, ff_suhu)
                        .commit();
                return true;

            case R.id.navigation_chart2:
                chart_suhu c_suhu = new chart_suhu();
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_suhu, c_suhu)
                        .commit();
                return true;

        }
        return false;
    }

}

