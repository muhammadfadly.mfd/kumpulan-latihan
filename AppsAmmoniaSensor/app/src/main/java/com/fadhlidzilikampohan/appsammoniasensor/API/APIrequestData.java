package com.fadhlidzilikampohan.appsammoniasensor.API;

import com.fadhlidzilikampohan.appsammoniasensor.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIrequestData {
    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();

    @GET("retrieve_kelembapan.php")
    Call<ResponseModel> ardRetrieveData2();

    @GET("retrieve_suhu.php")
    Call<ResponseModel> ardRetrieveData3();

    @GET("retrieve1.php")
    Call<ResponseModel> ardRetrieveData4();

}
