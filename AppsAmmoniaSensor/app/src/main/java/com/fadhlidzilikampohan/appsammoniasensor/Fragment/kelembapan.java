package com.fadhlidzilikampohan.appsammoniasensor.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fadhlidzilikampohan.appsammoniasensor.API.APIrequestData;
import com.fadhlidzilikampohan.appsammoniasensor.API.RetroServer;
import com.fadhlidzilikampohan.appsammoniasensor.Adapter.AdapterData;
import com.fadhlidzilikampohan.appsammoniasensor.Model.DataModel;
import com.fadhlidzilikampohan.appsammoniasensor.Model.ResponseModel;
import com.fadhlidzilikampohan.appsammoniasensor.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class kelembapan extends Fragment {

    private RecyclerView rvData;
    private RecyclerView.Adapter adData;
    private RecyclerView.LayoutManager lmData;
    private List<DataModel> listData = new ArrayList<>();
    private SwipeRefreshLayout srlData;
    private ProgressBar pbData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_kelembapan, container, false);

        return root;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        rvData = view.findViewById(R.id.rv_data);
        srlData = view.findViewById(R.id.srl_data);
        pbData = view.findViewById(R.id.pb_data);

        lmData = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvData.setLayoutManager(lmData);
        //retriverData();

        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                retriverData();
                srlData.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        retriverData();
    }

    public void retriverData() {
        pbData.setVisibility(View.VISIBLE);
        APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
        Call<ResponseModel> tampilData = ardData.ardRetrieveData2();
        tampilData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(getActivity(), "kode : " + kode + " | pesan :" + pesan, Toast.LENGTH_SHORT).show();

                listData = response.body().getData();

                adData = new AdapterData(getActivity(), listData);
                rvData.setAdapter(adData);
                adData.notifyDataSetChanged();

                pbData.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal Menghubungi Server : " + t.getMessage(), Toast.LENGTH_SHORT).show();
                pbData.setVisibility(View.INVISIBLE);
            }
        });
    }

}