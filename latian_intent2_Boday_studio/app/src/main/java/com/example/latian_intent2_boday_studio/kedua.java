package com.example.latian_intent2_boday_studio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class kedua extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kedua);
    }

    public void kembali(View view) {
        Intent tentang= new Intent(kedua.this,
                MainActivity.class);
        startActivity(tentang);

    }
}
