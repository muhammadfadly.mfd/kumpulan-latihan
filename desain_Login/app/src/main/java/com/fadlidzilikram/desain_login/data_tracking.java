package com.fadlidzilikram.desain_login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class data_tracking extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_tracking);
    }

    public void cari(View view) {
        Intent intent = new Intent(data_tracking.this, data_stok_kebutuhan.class);
        startActivity(intent);
    }
}
