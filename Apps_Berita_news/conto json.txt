{
    "status": "ok",
    "totalResults": 38,
    "articles": [
        {
            "source": {
                "id": null,
                "name": "Cnnindonesia.com"
            },
            "author": "CNN Indonesia",
            "title": "Komunitas Pers Minta Kapolri Cabut Pasal 2d Maklumat soal FPI - CNN Indonesia",
            "description": "Komunitas Pers menilai pasal 2d Maklumat Kapolri tak sesuai semangat negara demokrasi yang menghargai hak masyarakat untuk memperoleh dan menyebarkan informasi.",
            "url": "https://www.cnnindonesia.com/nasional/20210101190028-20-588569/komunitas-pers-minta-kapolri-cabut-pasal-2d-maklumat-soal-fpi",
            "urlToImage": "https://akcdn.detik.net.id/visual/2019/11/21/9c4c2adc-d58a-4d24-906b-e3976aef54d2_169.jpeg?w=650",
            "publishedAt": "2021-01-01T12:15:06Z",
            "content": "Jakarta, CNN Indonesia -- Komunitas Pers meminta KapolriJenderal Idham Azis mencabut pasal 2d dalam MaklumatKapolri Nomor: Mak/1/I/2021 tentang Kepatuhan terhadap Larangan Kegiatan, Penggunaan Simbol… [+3962 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Tribunnews.com"
            },
            "author": "Sri Juliati",
            "title": "Daftar HP Android dan iPhone yang Tak Bisa Pakai WA pada 2021, Samsung Galaxy S2 hingga iPhone 4 - Tribunnews.com",
            "description": "Inilah daftar HP Android dan iPhone yang tidak bisa lagi menggunakan WhatsApp (WA) pada 2021. Ada Samsung Galaxy S2 hingga iPhone 4.",
            "url": "https://www.tribunnews.com/techno/2021/01/01/daftar-hp-android-dan-iphone-yang-tak-bisa-pakai-wa-pada-2021-samsung-galaxy-s2-hingga-iphone-4",
            "urlToImage": "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/ilustrasi-whatsapp11.jpg",
            "publishedAt": "2021-01-01T11:42:57Z",
            "content": null
        },
        {
            "source": {
                "id": null,
                "name": "Pikiran-rakyat.com"
            },
            "author": "Rifki Alanudin",
            "title": "Awal tahun 2021, Berikut Ini Daftar Harga Hp Xiaomi dengan Harga di Bawah Rp 2 Jutaan : ada Note 9 - Jurnal Garut - Pikiran Rakyat",
            "description": "dafatar harga hp xiaomi dibawah 2 jutaan yang bisa kamu miliki di awal tahun 2021",
            "url": "https://jurnalgarut.pikiran-rakyat.com/teknologi/pr-331200489/awal-tahun-2021-berikut-ini-daftar-harga-hp-xiaomi-dengan-harga-di-bawah-rp-2-jutaan-ada-note-9",
            "urlToImage": "https://assets.pikiran-rakyat.com/crop/0x0:0x0/750x500/photo/2020/12/29/3018785649.jpg",
            "publishedAt": "2021-01-01T11:15:45Z",
            "content": "JURNAL GARUT - Xiaomi merupakan smartphone merek Cina yang paling populer dibanding dengan merek lainnya yang ada dipasaran.\r\npada artikel kali ini kami akan memberikan informasi terkait daftar harga… [+679 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Indeksnews.com"
            },
            "author": null,
            "title": "Ubur-Ubur Jeli Sisir, Temuan Baru Di Kedalaman 2,5 Mil - IndeksNews",
            "description": "Tim peneliti NOAA Fisheries menemukan Duobrachium sparksae, spesies baru ctenophore, atau jeli sisir. Penemuan ini dilakukan selama ekspedisi bawah air yang",
            "url": "https://indeksnews.com/ubur-ubur-jeli-sisir-temuan-baru-di-kedalaman-25/",
            "urlToImage": "https://indeksnews.com/wp-content/uploads/2021/01/PhotoRealisticDrawings_Cteno_NB2.png",
            "publishedAt": "2021-01-01T11:05:32Z",
            "content": "National Oceanic and Admospheric Administration (NOAA) menemukan ubur-ubur dari filum ctenophores yakni jeli sisir di kedalaman 2,5 mil menggunakan Deep Discoverer NOAA yang dioperasikan dari jarak j… [+3488 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Suara.com"
            },
            "author": "Dythia Novianty",
            "title": "Menuju Peluncuran, Samsung Rilis Video Teaser Galaxy S21? - Suara.com",
            "description": "Samsung merilis video teaser berisi kilas balik seri Galaxy S.",
            "url": "https://www.suara.com/tekno/2021/01/01/180000/menuju-peluncuran-samsung-rilis-video-teaser-galaxy-s21",
            "urlToImage": "https://media.suara.com/pictures/970x544/2019/09/03/11818-samsung-galaxy-s.jpg",
            "publishedAt": "2021-01-01T11:00:00Z",
            "content": "Suara.com - Samsung diketahui, merilis video teaser berisi kilas balik seri Galaxy S. Mengutip NDTV, Jumat (1/1/2021), video ini berada dalam channel Youtube Samsung Amerika Serikat, tapi masih dalam… [+1249 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Mediabogor.com"
            },
            "author": "Hamal Hamid",
            "title": "Astronot memakan lobak pertama yang ditanam di luar angkasa pada akhir tahun 2020 - Media Bogor",
            "description": "Stasiun Luar Angkasa Internasional difoto oleh anggota kru Ekspedisi 56 dari pesawat ruang angkasa Soyuz setelah dibongkar pada 4 Oktober 2018. Astronot",
            "url": "https://mediabogor.com/astronot-memakan-lobak-pertama-yang-ditanam-di-luar-angkasa-pada-akhir-tahun-2020/",
            "urlToImage": "https://cdnph.upi.com/sv/ph/og/upi_com/8791609439390/2021/1/58311d10fb493b26cd5d5e551f4838f9/v1.5/Astronauts-eat-first-radishes-grown-in-space-as-2020-ends.jpg",
            "publishedAt": "2021-01-01T10:58:56Z",
            "content": "Stasiun Luar Angkasa Internasional difoto oleh anggota kru Ekspedisi 56 dari pesawat ruang angkasa Soyuz setelah dibongkar pada 4 Oktober 2018. Astronot NASA Andrew Vostel, Ricky Arnold dan astronot … [+7281 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Kompas.com"
            },
            "author": "Nursita Sari",
            "title": "2 WNI Jadi Tersangka Kasus Parodi Lagu Indonesia Raya - Kompas.com - Nasional Kompas.com",
            "description": "Kedua tersangka berinisial NJ dan MDF. NJ ditangkap Polis Di-Raja Malaysia di Sabah, Malaysia; sedangkan MDF ditangkap Polri di Cianjur, Jawa Barat. Halaman all",
            "url": "https://nasional.kompas.com/read/2021/01/01/17554591/2-wni-jadi-tersangka-kasus-parodi-lagu-indonesia-raya",
            "urlToImage": "https://asset.kompas.com/crops/qmK43VxzVzcoEmeM_oIEGDA2XAM=/0x0:999x666/780x390/filters:watermark(data/photo/2020/03/10/5e6775ae18c31.png,0,-0,1)/data/photo/2020/12/28/5fe9949d372ee.jpg",
            "publishedAt": "2021-01-01T10:55:00Z",
            "content": "JAKARTA, KOMPAS.com - Kasus parodi lagu Indonesia Raya yang diunggah di kanal YouTube menyeret dua warga negara Indonesia (WNI) menjadi tersangka.\r\nKeduanya adalah NJ dan MDF.\r\nNJ merupakan seorang W… [+2517 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Detik.com"
            },
            "author": "Lucas Aditya",
            "title": "MU Vs Aston Villa: Martial Buru Gol Pertama ke Gawang The Villa - detikSport",
            "description": "Manchester United menjamu Aston Villa di lanjutan Liga Inggris. Penyerang Setan Merah, Anthony Martial, memburu gol pertama ke gawang The Villa.",
            "url": "https://sport.detik.com/sepakbola/liga-inggris/d-5317600/mu-vs-aston-villa-martial-buru-gol-pertama-ke-gawang-the-villa",
            "urlToImage": "https://akcdn.detik.net.id/api/wm/2020/11/23/anthony-martial-1_169.jpeg?wid=54&w=650&v=1&t=jpeg",
            "publishedAt": "2021-01-01T10:21:29Z",
            "content": "Jakarta - Manchester United menjamu Aston Villa di lanjutan Liga Inggris. Penyerang Setan Merah, Anthony Martial, memburu gol pertama ke gawang The Villa.\r\nMU vs Aston Villa berlangsung di Old Traffo… [+1272 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Kompas.com"
            },
            "author": "Eris Eka Jaya",
            "title": "Man United Punya Senjata Rahasia, Bisa Tikung Liverpool! - Kompas.com - KOMPAS.com",
            "description": "Manchester United disebut memiliki senjata rahasia untuk bersaing dengan Liverpool pada Liga Inggris musim 2020-2021.",
            "url": "https://bola.kompas.com/read/2021/01/01/17172738/man-united-punya-senjata-rahasia-bisa-tikung-liverpool",
            "urlToImage": "https://asset.kompas.com/crops/DNyA0tLTTJRJmC31c5cCQ-5HSwY=/241x0:950x472/780x390/filters:watermark(data/photo/2020/03/10/5e6775b55942a.png,0,-0,1)/data/photo/2020/11/05/5fa32b6c81dfd.jpg",
            "publishedAt": "2021-01-01T10:17:00Z",
            "content": "KOMPAS.com - Manchester United yang tengah bersaing dengan Liverpool di papan atas klasemen Liga Inggris musim 2020-2021 disebut memiliki senjata rahasia.\r\nSenjata rahasia itu terwujud lewat kepiawai… [+1258 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Jpnn.com"
            },
            "author": "JPNN.com",
            "title": "Front Persatuan Islam Dibentuk, Mahfud MD Beri Komentar Begini - JPNN.com",
            "description": "JPNN.com : Menteri Koordinator bidang Politik, Hukum dan Keamanan, Mahfud MD, memberikan tanggapan terkait pendirian Front Persatuan Islam.",
            "url": "https://www.jpnn.com/news/front-persatuan-islam-dibentuk-mahfud-md-beri-komentar-begini",
            "urlToImage": "https://photo.jpnn.com/arsip/watermark/2020/12/16/menko-polhukam-mahfud-md-foto-fathan-sinagajpnncom-41.png",
            "publishedAt": "2021-01-01T09:51:00Z",
            "content": "jpnn.com, JAKARTA - Menteri Koordinator bidang Politik, Hukum dan Keamanan, Mahfud MD, memberikan tanggapan terkait pendirian Front Persatuan Islam.\r\nIa mengatakan pihaknya tidak melarang pendirian F… [+2292 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Mediautama.co"
            },
            "author": "Barnabas Keliat",
            "title": "√ Rumus Mikroskop untuk Perbesaran Benda - MEDIA UTAMA",
            "description": "Saat Anda melihat benda kecil di bawah mikroskop seperti protozoa atau jaringan pada tanaman, Anda mungkin ingin tahu berapa ukurannya, bukan? Hasil pengamatan ternyata berukuran sekitar 1 hingga 2 cm. Tentu saja benda atau benda tersebut memiliki ukuran asli…",
            "url": "https://mediautama.co/√-rumus-mikroskop-untuk-perbesaran-benda/",
            "urlToImage": "https://1.bp.blogspot.com/-dT-IttLiLAc/X-7iwUrDViI/AAAAAAAAAJ8/dlAMdUQv7o4EXyqmsUKRzbSGvSMX5xECwCLcBGAsYHQ/w1200-h630-p-k-no-nu/042046700_1552028850-Lab-Budidaya-Ikan-Hias1.jpg",
            "publishedAt": "2021-01-01T09:13:06Z",
            "content": "Bulan purnama Dingin rupanya muncul di langit Indonesia pada 30 Desember 2020 kemarin. Menurut LAPAN, puncak bulan purnama terjadi pada pukul 10.00 WIB dan untuk wilayah Indonesia Tengah dan Timur bu… [+4044 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Kompas.com"
            },
            "author": "Novianti Setuningsih",
            "title": "Son Ye Jin Ungkap Perasaan Usai Konfirmasi Pacaran dengan Hyun Bin - Kompas.com - KOMPAS.com",
            "description": "Son Ye Jin dalam unggahannya mengaku malu karena di awal tahun 2021 muncul dengan kabar kehidupan pribadi dan bukan soal pekerjaannya",
            "url": "https://www.kompas.com/hype/read/2021/01/01/160427166/son-ye-jin-ungkap-perasaan-usai-konfirmasi-pacaran-dengan-hyun-bin",
            "urlToImage": "https://asset.kompas.com/crops/4ollwQYRaTnXAai3vscHH6UkFVE=/0x0:900x600/780x390/filters:watermark(data/photo/2020/03/10/5e6775d943eeb.png,0,-0,1)/data/photo/2019/01/21/1427526452.jpeg",
            "publishedAt": "2021-01-01T09:04:00Z",
            "content": "KOMPAS.com - Aktris Korea Selatan, Son Ye Jin akhirnya buka suara usai mengonfirmasi tengah berpacaran dengan lawan mainnya di drama Crash Landing On You, Hyun Bin.\r\nMelalui unggahan di akun Instagra… [+1774 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Tribunnews.com"
            },
            "author": "Suci Bangun Dwi Setyaningsih",
            "title": "Live Streaming Ikatan Cinta RCTI Jumat, 1 Januari 2021: Al Khawatir Kondisi Andin, Angga Ganggu Al - Tribunnews.com",
            "description": "Berikut link live streaming nonton Sinetron Ikatan Cinta malam ini, pukul 19.30 WIB, Jumat (1/1/2021), tentang Al yang khawatir kondisi Andin.",
            "url": "https://www.tribunnews.com/seleb/2021/01/01/live-streaming-ikatan-cinta-rcti-jumat-1-januari-2021-al-khawatir-kondisi-andin-angga-ganggu-al",
            "urlToImage": "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/cuplikan-video-ikatan-cinta-rcti-jumat-1-januari-20211.jpg",
            "publishedAt": "2021-01-01T08:56:06Z",
            "content": "TRIBUNNEWS.COM - Berikut link live streaming nonton sinetron Ikatan Cinta malam ini, pukul 19.30 WIB, Jumat (1/1/2021).\r\nCuplikan tayangan sinetron Ikatan Cinta untuk malam ini memperlihatkan Al seda… [+891 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Detik.com"
            },
            "author": "Firdaus Anwar",
            "title": "Update Corona di Indonesia: Tambah 8.072 Kasus di Hari Pertama 2021 - detikHealth",
            "description": "Memasuki tahun baru 2021, kasus Corona di Indonesia hari ini bertambah 8.072 kasus. Total kasus positif menjadi 751.270, sembuh 617.936, dan meninggal 22.329.",
            "url": "https://health.detik.com/berita-detikhealth/d-5317575/update-corona-di-indonesia-tambah-8072-kasus-di-hari-pertama-2021",
            "urlToImage": "https://awsimages.detik.net.id/api/wm/2020/12/29/mural-lawan-covid-19-mejeng-di-cakung-7_169.jpeg?wid=54&w=650&v=1&t=jpeg",
            "publishedAt": "2021-01-01T08:49:54Z",
            "content": "Jakarta - Jumlah kasus virus Corona COVID-19 bertambah 8.072 pada Jumat (1/1/2021). Total kasus positif menjadi 751.270, sembuh 617.936, dan meninggal 22.329.\r\nSementara itu jumlah spesimen yang dipe… [+518 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Inews.id"
            },
            "author": "Andika Pratama",
            "title": "Cristiano Ronaldo Sebut Pacaran dengan Irina Shayk Bagai Ada di Surga Dunia - iNews",
            "description": "Striker Juventus Cristiano Ronaldo tampaknya masih memikirkan Irina Shayk. Buktinya, dia mengutarakan indahnya masa pacaran dengan model asal Rusia itu.",
            "url": "https://www.inews.id/sport/soccer/cristiano-ronaldo-sebut-pacaran-dengan-irina-shayk-bagai-ada-di-surga-dunia",
            "urlToImage": "https://img.inews.co.id/media/600/files/inews_new/2021/01/01/cristiano_ronaldo_irina_shayk.jpg",
            "publishedAt": "2021-01-01T08:49:00Z",
            "content": "TURIN, iNews.id – Striker Juventus Cristiano Ronaldo tampaknya masih memikirkan Irina Shayk. Buktinya, dia mengutarakan indahnya masa pacaran dengan model asal Rusia itu. \r\nHubungan dua sejoli ini pe… [+1031 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Detik.com"
            },
            "author": "Asep Syaifullah",
            "title": "Gading Habiskan Malam Tahun Baru dengan Karen Nijsen, Netter: Calon Mama Gempi - detikHot",
            "description": "Gading Marten menghabiskan momen pergantian tahun baru bersama Gempi, Karen Nijsen, dan teman-temannya. Netizen sibuk mengomentari sebagai calon mama Gempi.",
            "url": "https://hot.detik.com/celeb/d-5317557/gading-habiskan-malam-tahun-baru-dengan-karen-nijsen-netter-calon-mama-gempi",
            "urlToImage": "https://awsimages.detik.net.id/api/wm/2021/01/01/gading-marten-dan-gempi_169.png?wid=54&w=650&v=1&t=jpeg",
            "publishedAt": "2021-01-01T08:43:49Z",
            "content": "Jakarta - Sosok Karen Nijsen kini tengah jadi sorotan usai kedekatannya dengan Gading Marten tercium awak media. Kisah asmara mereka pun selalu jadi bahasan di berbagai media.\r\nKaren pun seolah menja… [+1930 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Kompas.tv"
            },
            "author": "Reny Mardika",
            "title": "Vaksin Covid-19 Pfizer-BioNTech Resmi Kantongi Persetujuan dari WHO - Kompas TV",
            "description": "Vaksin Covid-19 buatan duet Amerika Serikat dan Jerman, Pfizer-BioNTech, menjadi vaksin pertama yang mengantongi persetujuan penggunaan darurat dari WHO.",
            "url": "https://www.kompas.tv/article/134754/vaksin-covid-19-pfizer-biontech-resmi-kantongi-persetujuan-dari-who",
            "urlToImage": "https://media-origin.kompas.tv/library/image/thumbnail/1609484809160/1609484809160.jpg",
            "publishedAt": "2021-01-01T08:15:00Z",
            "content": "JAKARTA, KOMPAS.TV - Vaksin Covid-19 buatan duet Amerika Serikat dan Jerman, Pfizer-BioNTech, menjadi vaksin pertama yang mengantongi persetujuan penggunaan darurat dari badan kesehatan dunia (WHO).\r… [+1197 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Pikiran-rakyat.com"
            },
            "author": "Tim Portal Probolinggo 02",
            "title": "Saat Nyeri Asam Urat Sudah Tak Tertahankan, Coba Obati dengan 6 Pengobatan Rumahan Ini - Portal Probolinggo - Pikiran Rakyat",
            "description": "6 pengobatan rumahan ini dapat menjadi obat ketika nyeri asam urat datang.",
            "url": "https://portalprobolinggo.pikiran-rakyat.com/gaya-hidup/pr-781199264/saat-nyeri-asam-urat-sudah-tak-tertahankan-coba-obati-dengan-6-pengobatan-rumahan-ini",
            "urlToImage": "https://assets.pikiran-rakyat.com/crop/0x0:0x0/750x500/photo/2020/12/06/1591484227.jpg",
            "publishedAt": "2021-01-01T07:21:03Z",
            "content": "PORTAL PROBOLINGGO - Pemecahan purin dapat menyebabkan penumpukan asam urat, hal inilah asal mula terjadinya penyakit asam urat.\r\nPenderita akan mengalami nyeri hebat dan bengkak serta persendian ter… [+643 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Detik.com"
            },
            "author": "Puti Yasmin",
            "title": "Cek PLN Token Gratis Januari 2021 di www.pln.co.id atau WA 08122123123 - detikFinance",
            "description": "Cara cek PLN token gratis 2021 bisa dilakukan dengan mengakses www.pln.co.id gratis token atau dengan WA di nomor 08122123123. Begini tahapannya.",
            "url": "https://finance.detik.com/energi/d-5317407/cek-pln-token-gratis-januari-2021-di-wwwplncoid-atau-wa-08122123123",
            "urlToImage": "https://awsimages.detik.net.id/api/wm/2015/09/09/b0b4d649-15c9-4307-8759-f57be55c7d59_169.jpg?wid=54&w=650&v=1&t=jpeg",
            "publishedAt": "2021-01-01T07:00:10Z",
            "content": "Jakarta - Pemerintah memberikan stimulus Covid-19 berupa PLN token gratis kepada masyarakat yang terdampak. Program ini sebelumnya telah berjalan di tahun 2020 dan dipastikan berlanjut hingga 2021.\r\n… [+1465 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Detik.com"
            },
            "author": "Marlinda Oktavia Erwanti",
            "title": "Inggris Resmi Tinggalkan Uni Eropa, PM Boris Johnson: Momen Luar Biasa - detikNews",
            "description": "Hari ini, Inggris memulai era baru. Era baru di Britania Raya itu dimulai setelah resmi berpisah dari Uni Eropa.",
            "url": "https://news.detik.com/internasional/d-5317421/inggris-resmi-tinggalkan-uni-eropa-pm-boris-johnson-momen-luar-biasa",
            "urlToImage": "https://awsimages.detik.net.id/api/wm/2020/11/16/files-britain-eu-brexit-trade-politics_169.jpeg?wid=54&w=650&v=1&t=jpeg",
            "publishedAt": "2021-01-01T06:11:10Z",
            "content": "London - Hari ini, Inggris memulai era baru. Era baru di Britania Raya itu dimulai setelah resmi berpisah dari Uni Eropa.\r\nInggris berhenti mengikuti aturan UE pada pukul 23.00 waktu setempat, Kamis … [+1979 chars]"
        }
    ]
}