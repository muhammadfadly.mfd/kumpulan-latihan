package com.fadhlidzilikampohan.apps_notifv3;


import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.util.Log;


import androidx.core.app.ActivityCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "FirebaseInstanceIDService";



    @SuppressLint("LongLogTag")
    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token : " + token);
        registerToken(token);
    }

    private void registerToken(String token) {



        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("Token",token)

                .build();

        Request request = new Request.Builder()
                .url("https://passnyacoba123tandaseru.000webhostapp.com/firebasev1/register.php")
                .post(body)
                .build();

        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;

            default:
                break;
        }
    }

}
